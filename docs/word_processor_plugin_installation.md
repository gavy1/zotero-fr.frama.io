# Installer les modules pour logiciel de traitement de texte de Zotero

_Consulter cette page dans la documentation officielle de Zotero : [Installing the Zotero Word Processor Plugins](https://www.zotero.org/support/word_processor_plugin_installation) - dernière mise à jour de la traduction : 2022-06-30_

Les [modules pour logiciel de traitement de texte](./word_processor_integration.md) sont fournis avec Zotero et doivent être installés automatiquement pour chaque [logiciel de traitement de texte pris en charge (page en anglais)](https://www.zotero.org/support/system_requirements#word_processor_plugins) installé sur votre ordinateur lorsque vous démarrez Zotero pour la première fois.

Vous pouvez réinstaller ultérieurement ces modules depuis l'onglet Citer -&gt; Traitements de texte dans les préférences de Zotero. Si vous rencontrez des difficultés, consultez les pages [Installer manuellement les modules pour logiciel de traitement de texte de Zotero](./word_processor_plugin_manual_installation.md) ou [Dépannage des modules pour logiciel de traitement de texte](https://www.zotero.org/support/word_processor_plugin_troubleshooting).

Si vous avez déjà installé les versions Firefox des modules pour logiciel de de traitement de texte dans Zotero 5.0 ou Zotero Standalone 4.0, vous devez les désinstaller à partir de Outils -&gt; Extensions.
