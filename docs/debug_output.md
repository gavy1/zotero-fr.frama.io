# Enregistrement des journaux de débogage

*Consulter cette page dans la documentation officielle de Zotero : [Debug Output Logging](https://www.zotero.org/support/debug_output) - dernière mise à jour de la traduction : 2022-07-08*

Si on vous a demandé de fournir un "Debug ID" (qui est différent d'un ["Report ID"](./reporting_problems.md) ) pour aider à résoudre un problème, suivez ces étapes simples.

### Zotero

1.  Dans le menu "Aide", sélectionnez "Journal de débogage" puis "Activer", ou utilisez "Redémarrer avec le journal activé" pour générer des journaux de débogage à partir du démarrage de Zotero. (Si vous n'arrivez pas à accéder au menu "Aide", reportez-vous plutôt à [Signaler des erreurs de démarrage](./reporting_problems.md#signaler-des-erreurs-de-demarrage).)
2.  Effectuez immédiatement l'action qui pose problème (synchronisation, sauvegarde, importation, etc.) et reproduisez le problème que vous rencontrez.
3.  Avant de faire quoi que ce soit d'autre, retournez dans le menu "Aide" -&gt; "Journal de débogage" et cliquez sur "Envoyer le journal", ce qui désactivera l'enregistrement et enverra le journal à zotero.org. Une fenêtre contenant un Debug ID (par exemple, "D12345678") devrait apparaître. Cliquez sur "Copier dans le presse-papiers" et collez le Debug ID dans le fil de discussion que vous avez créé sur le forum.

Si l'envoi du journal échoue, vous pouvez retourner au menu "Journal de débogage" et sélectionner "Voir le journal", puis allez à Fichier → "Enregistrer...", choisissez le format (ou Type) : "Fichiers textes" et enregistrez le journal dans un fichier. Vous pouvez envoyer ce fichier par courriel à support@zotero.org, avec un lien vers votre fil de discussion du forum. Il peut être utile de compresser le fichier avant de l'envoyer par courriel.


### Connecteurs Zotero (Firefox, Chrome et Safari)

1.  Ouvrez les préférences du connecteur Zotero.
    -   **Firefox:** cliquez avec le bouton droit de la souris sur l'icône du connecteur dans la barre d'outils de Firefox et cliquez sur "Preferences".
    -   **Chrome:** cliquez avec le bouton droit de la souris sur l'icône du connecteur dans la barre d'outils de Chrome et cliquez sur "Options".
    -   **Safari:** cliquez avec le bouton droit n'importe où sur une page web et sélectionnez "Zotero Preferences...".
2.  Dans l'onglet "Advanced" des préférences du connecteur de Zotero, cochez la case "Enable logging". Ne fermez pas cette fenêtre ou cet onglet.
3.  Effectuez immédiatement toutes les actions qui posent problème (par exemple, importer un élément à partir d'une page Web).
4.  Retournez à l'onglet "Advanced" des préférences du connecteur de Zotero et cliquez sur le bouton "Submit Output".
5.  Un Debug ID (par exemple, "D12345678") vous sera fourni. Veuillez poster ce Debug ID sur le forum.
6.  Décochez la case "Enable logging".

### Zotero pour iOS

1. Dans le volet de gauche, appuyez sur "Back" jusqu'à ce que vous voyiez la liste des bibliothèques
2. Appuyez sur l'icône d'engrenage dans le coin supérieur droit de ce volet.
3. Appuyez sur "Debug Output Logging" et ensuite sur "Start Logging".
4. Fermez la fenêtre des paramètres.
5. Effectuez immédiatement toutes les actions pertinentes (par exemple, en tirant vers le bas sur la liste des documents pour déclencher une synchronisation).
6. Lorsque vous avez terminé, appuyez sur le bouton circulaire d'arrêt dans le coin inférieur gauche de l'écran.
7. Dans l'alerte qui s'affiche, appuyez sur "Copy" pour copier le Debug ID dans le presse-papiers, puis collez-le dans votre fil de discussion.


## Connexion à une fenêtre de terminal

Si vous souhaitez suivre régulièrement le journal de débogage de Zotero en temps réel, il peut être préférable de connecter Zotero à une fenêtre de terminal.

### macOS

-   Ouvrez Terminal via Spotlight ou /Applications/Utilities.
-   Collez `/Applications/Zotero.app/Contents/MacOS/zotero -ZoteroDebugText` dans la fenêtre Terminal.
-   Appuyez sur Entrée.

### Windows

-  Ouvrez cmd.exe
-   Collez dans la fenêtre de console :
```
“C:\Program Files (x86)\Zotero\zotero.exe” -ZoteroDebugText -console
```
-   Appuyez sur Entrée.

En raison des limitations des fenêtres de console disponibles sous Windows, nous recommandons de commencer plutôt avec `-ZoteroDebug`, pour utiliser la fenêtre de sortie de débogage interne de Zotero. 

### Linux

Lancez Zotero en ligne de commande, en ajoutant à l'invite de commande `-ZoteroDebug`.

Pour capturer la sortie lorsque Zotero se bloque ou se suspend, vous pouvez utiliser `-ZoteroDebugText > zotero-debug.txt` pour rediriger la sortie vers un fichier.

#### Note pour les développeurs

Pour activer le journal de débogage de Zotero en permanence, paramétrez extensions.zotero.debug.log sur true dans l'éditeur de configuration de Zotero, accessible depuis le menu Préférences &gt; Avancées de Zotero, puis lancez Zotero en ligne de commande.
