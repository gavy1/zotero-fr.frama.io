# Installer manuellement les modules pour logiciel de traitement de texte de Zotero

*Consulter cette page dans la documentation officielle de Zotero : [Manually Installing the Zotero Word Processor Plugin](https://www.zotero.org/support/word_processor_plugin_manual_installation) - dernière mise à jour de la traduction : 2022-06-08*

Les modules de traitement de texte de Zotero s'installent automatiquement pour la plupart des utilisateurs. Si vous ne voyez pas la barre d'outils Zotero dans votre traitement de texte, vous devez [essayer de réinstaller le module](https://www.zotero.org/support/word_processor_plugin_troubleshooting#zotero_toolbar_doesn_t_appear) depuis le volet Citer → Traitements de texte des préférences Zotero. Si vous recevez une erreur ou ne voyez toujours pas le module après avoir essayé de le réinstaller depuis les préférences, vous pouvez essayer les instructions d'installation manuelle ci-dessous.

Notez que si vous vous fiez à l'installation manuelle, vous risquez de rencontrer des problèmes plus tard en raison de l'absence de mise à jour du module. Il est donc préférable de déterminer pourquoi l'installation automatique ne fonctionne pas (par exemple, un logiciel de sécurité bloquant l'installation ou un emplacement incorrect du dossier de démarrage du traitement de texte) et de résoudre le problème sous-jacent.


## Word pour Windows

1.  Ouvrez le dossier d'installation de Zotero (généralement C:\\Program Files (x86)\\Zotero).
2.  Dans le dossier d'installation, ouvrez `extensions\\zoteroWinWordIntegration@zotero.org\\install`, où vous trouverez une copie du fichier Zotero.dotm. 
    * Si le dossier est vide, le fichier a été supprimé d'une manière ou d'une autre - peut-être par un logiciel de sécurité - et vous devez réinstaller Zotero. 
    * Si le dossier est vide immédiatement aprèsa voir résintallé Zotero, vous pouvez [télécharger Zotero.dotm](https://github.com/zotero/zotero-word-for-windows-integration/raw/master/install/Zotero.dotm), mais votre logiciel de sécurité peut supprimer de la même façon ce fichier téléchargé, et vous devrez le configurer pour qu'il n'effectue pas cette suppression.
    * Si vous voyez deux fichiers "Zotero" sans extension de fichier, votre ordinateur est réglé pour ne pas afficher les extensions de fichier. Vous pouvez déterminer lequel est Zotero.dotm en cliquant avec le bouton droit de la souris sur chaque fichier et en sélectionnant "Propriétés". L'un sera “Modèle Word 97-2003 (.dot)” and one will say “Modèle Word prenant en charge les macros (.dotm)”.
3.  Trouvez votre dossier de démarrage Word et copiez le chemin dans le presse-papiers.
    * Dans le ruban de Word, cliquez sur l'onglet "Fichier", puis sur "Options" et enfin sur "Options avancées"
    * Sous "Général", cliquez sur "Emplacements des fichiers...". L'emplacement actuel du dossier de démarrage" doit y être répertorié. Dans la plupart des cas, le chemin du dossier de démarrage doit être l'emplacement par défaut suivant : `C:\\Users\\::nom d'utilisateur::\\AppData\\Roaming\\Microsoft\\Word\\STARTUP` (ou `Startup`), où `::nom d'utilisateur::` est votre nom d'utilisateur sur votre ordinateur. Le chemin ne doit en aucun cas inclure "Zotero", et si c'est le cas, c'est que vous l'avez configuré de manière incorrecte. Si c'est le cas, vous devez réinitialiser le chemin à l'emplacement par défaut.
    * Sélectionnez le chemin du dossier de démarrage et cliquez sur "Modifier", cliquez dans l'espace à droite du chemin dans la barre d'adresse en haut de la fenêtre, copiez le chemin complet dans le presse-papiers avec Ctrl-C, puis **cliquez sur Annuler** pour fermer la boîte de dialogue sans apporter de modifications.

4. Ouvrez une nouvelle fenêtre de votre explorateur de fichiers et collez le chemin du dossier de démarrage dans la barre d'adresse. Vous devez maintenant avoir deux dossiers ouverts : le dossier "install" contenant Zotero.dotm et le dossier de démarrage de Word.
4.  Copiez le fichier Zotero.dotm dans votre dossier de démarrage de Word. Veillez à copier le fichier plutôt que de le déplacer. Si vous le faites glisser, activez la touche Ctrl.
5.  Redémarrez Word pour commencer à utiliser le module.

## Word pour Mac 2016 et 2019

1.  Dans Finder, appuyez sur Cmd-Shift-G et naviguez jusqu'à `/Applications/Zotero.app/Contents/Resources/extensions/zoteroMacWordIntegration@zotero.org/install` où vous trouverez une copie du fichier Zotero.dotm. Si le dossier est vide, le fichier a été supprimé d'une manière ou d'une autre - peut-être par un logiciel de sécurité - et vous devez réinstaller Zotero.
2.  Trouvez votre dossier de démarrage Word en suivant les instructions ci-dessous. Vous devez maintenant avoir deux dossiers ouverts : le dossier de démarrage de Word et le dossier "install" contenant Zotero.dotm.
3.  Copiez le fichier Zotero.dotm dans votre dossier de démarrage de Word. (Veillez à copier le fichier plutôt que de le déplacer).
4.  Démarrez (ou redémarrez) Microsoft Word pour commencer à utiliser le module.

## Word pour Mac 2011

1.  Téléchargez [Zotero.dot.zip](https://download.zotero.org/integration/macword/dot/Zotero.dot-5.0.5.zip) et double-cliquez dessus pour extraire le fichier Zotero.dot. (Remarque : le fichier doit être extrait dans le Finder, et non via la ligne de commande).
2.  Trouvez votre dossier de démarrage Word en suivant les instructions ci-dessous.
3.  Déplacez le fichier Zotero.dot dans votre dossier de démarrage Word.
    1.  **Si vous avez une version non anglaise d'Office**, vous devrez peut-être déplacer le fichier Zotero.dot dans le répertoire "Word" dans l'équivalent de "Startup" dans votre langue ("Démarrage" en français). Le chemin d'accès correct doit être indiqué dans Word, dans Outils -&gt; Modèles et compléments.
    2.  **Si vous utilisez un compte utilisateur macOS non administrateur**, vous devrez installer le module Word à partir d'un compte administrateur, ou accorder un accès en écriture pour le répertoire de démarrage au compte non administrateur, afin de lui permettre d'installer Zotero.dot.
4.  Démarrez (ou redémarrez) Microsoft Word pour commencer à utiliser le module.
5.  Supprimez le fichier ZIP téléchargé.

## LibreOffice

1.  Accédez aux fichiers d'application de Zotero.
    -   Mac : Dans le Finder, appuyez sur Cmd-Shift-G et collez `/Applications/Zotero.app/Contents/Resources/extensions/zoteroOpenOfficeIntegration@zotero.org/install`
    -   Windows : Ouvrez le dossier `C:\\Program Files (x86)\\Zotero\\extensions\\zoteroOpenOfficeIntegration@zotero.org/install`
    -   Linux : Allez dans le répertoire où Zotero est installé et ouvrez `extensions/zoteroOpenOfficeIntegration@zotero.org/install`
2.  Double-cliquez sur le fichier Zotero\_OpenOffice\_Integration.oxt pour l'installer.

Si vous obtenez un message d'erreur, il y a un problème avec votre installation LibreOffice, et vous devez suivre [les étapes de dépannage](https://www.zotero.org/support/word_processor_plugin_troubleshooting#installation_error).

## Localiser votre dossier de démarrage Word

Remarque : dans des systèmes dans une langue autre que l'anglais ou dans certaines configurations personnalisées, ces emplacements peuvent être différents.

#### Word 2007 ou supérieur pour Windows

L'emplacement par défaut du dossier de démarrage est le suivant : `C:\\Users\\::nom d'utilisateur::\\AppData\\Roaming\\Microsoft\\Word\\Startup`, où `::nom d'utilisateur::` est votre nom d'utilisateur sur votre ordinateur. Le dossier `AppData` est peut-être caché sur votre système, mais vous pouvez y accéder en ouvrant l'explorateur de fichiers de Windows, en tapant `%AppData%` dans la barre d'adresse et en appuyant sur Entrée, ce qui vous amènera dans le répertoire `Roaming`. De là, vous pouvez naviguer jusqu'à Microsoft\Word\Startup. 

Si les modifications que vous apportez au dossier de démarrage ne prennent pas effet, vous pouvez confirmer que Word n'est pas défini à un autre emplacement. Dans le ruban de Word, cliquez sur l'onglet "Fichier", puis sur "Options", et enfin sur "Options avancées". Sous "Général", cliquez sur "Emplacements des fichiers". Le dossier de démarrage devrait y figurer. Sélectionnez-le et cliquez sur "Modifier". Dans la fenêtre qui s'ouvre, cliquez sur l'espace à droite du chemin dans la barre d'adresse en haut et copiez le chemin complet dans le presse-papiers en appuyant sur Ctrl-C. **Cliquez sur "Annuler"** pour fermer la boîte de dialogue sans effectuer de modifications. Vous pouvez ensuite ouvrir une nouvelle boîte de dialogue de l'explorateur de fichiers et coller le chemin d'accès dans la barre d'adresse pour ouvrir le dossier de démarrage. 

Notez que le chemin ne doit en aucun cas inclure "Zotero", et si c'est le cas c'est que vous l'avez configuré de manière incorrecte. Si c'est le cas, vous devez réinitialiser le chemin à l'emplacement par défaut.


#### Word 2016 et 2019 pour Mac

L'emplacement par défaut est `~/Library/Group Containers/UBF8T346G9.Office/User Content/Startup/Word`.  (~/Library désigne le dossier Library de votre répertoire personnel.) Vous pouvez l'ouvrir à partir du Finder en appuyant sur Cmd-Shift-G et en copiant le chemin. Vous pouvez également y accéder dans le Finder en maintenant la touche "Option" enfoncée, en cliquant sur le menu "Aller", en sélectionnant "Library" (qui est caché par défaut), puis en suivant le reste du chemin.

Si les modifications apportées au dossier de démarrage ne sont pas prises en compte, vous pouvez vérifier que Word n'est pas à un autre emplacement. Dans Word, ouvrez le menu "Word" en haut à gauche de l'écran et sélectionnez "Préférences". Cliquez sur "Emplacements des fichiers" sous "Paramètres personnels" et cliquez sur "Démarrage" en bas de la liste.

En général, aucun emplacement ne doit être indiqué, ce qui amène Word à utiliser l'emplacement par défaut. Si un autre emplacement est indiqué (par exemple, /Applications/Microsoft Office 2011/Office/Startup/Word, à partir d'une version antérieure de Word), effacer le paramètre et laisser Word utiliser l'emplacement par défaut peut résoudre les problèmes d'installation et permettre à Zotero d'installer le module automatiquement à l'avenir.

Notez que le chemin ne doit en aucun cas inclure "Zotero", et si c'est le cas c'est que vous l'avez configuré de manière incorrecte. Si c'est le cas, vous devez réinitialiser le chemin pour qu'il soit vide et que l'emplacement par défaut soit utilisé. 

#### Word 2011 pour Mac

L'emplacement par défaut du dossier de démarrage est /Applications/Microsoft Office 2011/Office/Startup/Word. Vous pouvez l'ouvrir à partir du Finder en appuyant sur Cmd-Shift-G et en copiant le chemin d'accès ou en y accédant par navigation.

Si les modifications que vous apportez au dossier de démarrage ne prennent pas effet, vous pouvez confirmer que Word n'est pas localisé à un autre emplacement. Dans Word, ouvrez le menu "Word" en haut à gauche de l'écran et sélectionnez "Préférences". Cliquez sur "Emplacements des fichiers" sous "Paramètres personnels" et cliquez sur "Démarrage" en bas de la liste.

