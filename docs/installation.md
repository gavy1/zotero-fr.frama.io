---
hide:
  - navigation
---
# Installation

*Consulter cette page dans la documentation officielle de Zotero : [Installation instructions](https://www.zotero.org/support/installation) - dernière mise à jour de la traduction : 2022-07-04*

## Où télécharger Zotero ?

Vous pouvez télécharger Zotero depuis [la page de téléchargement](https://zotero.org/download/) (en anglais). Assurez-vous d'installer aussi un connecteur Zotero pour votre navigateur.

## Comment installer Zotero ?

### Mac

Ouvrez le fichier .dmg que vous avez téléchargé et déposez Zotero dans le dossier Applications. Vous pouvez ensuite lancer Zotero depuis Spotlight, Launchpad ou le dossier Applications, et l'ajouter à votre Dock comme n'importe quel programme.

Après avoir installé Zotero, vous pouvez éjecter et supprimer le fichier .dmg.

### Windows

Ouvrez le programme d'installation (setup) que vous avez téléchargé.

### Linux

Téléchargez le tarball, dézippez-le et ouvrez "zotero" depuis le répertoire obtenu afin de démarrer Zotero.

Pour Ubuntu, le tarball inclut un fichier .desktop qui peut être utilisé pour ajouter Zotero au lanceur. Déplacez le répertoire dézippé vers un emplacement de votre choix (par exemple `/opt/zotero`), exécutez le script `set_launcher_icon` depuis un terminal afin de mettre à jour le fichier .desktop pour cet emplacement, et créez un lien symbolique (symlink) entre `zotero.desktop` et `~/.local/share/applications/` (par exemple si vous avez placé Zotero dans `/opt/zotero` : `ln -s /opt/zotero/zotero.desktop ~/.local/share/applications/zotero.desktop`). Zotero devrait alors apparaître soit dans votre lanceur ou dans la liste des applications lorsque vous cliquez sur l’icone grille ("Afficher les application"), à partir de laquelle vous pouvez la glisser-déposer dans le lanceur.

#### Distributions basées sur Debian / Ubuntu


Un contributeur au long-terme maintient [zotero-deb](https://github.com/retorquere/zotero-deb), un wrapper pour le tarball officiel.

#### Autres paquets

Des paquets non-officiels sont aussi disponibles pour différentes distributions, mais notez que ces paquets sont crées par des tiers, et que Zotero ne propose du support que pour le tarball officiel et le paquet zotero-deb. En particulier, les paquets tiers peuvent être sandboxés, ce qui a pour effet de "casser" certaines fonctionnalités. 

#### Chromebook

Pour installer Zotero sur un Chromebook, voir [Installer sur un Chromebook](https://www.zotero.org/support/kb/installing_on_a_chromebook)

## Comment mettre à jour Zotero vers une nouvelle version ?

Zotero devrait se mettre à jour automatiquement, mais vous pouvez aussi le faire manuellement en allant dans le menu Aide et en choisissant "Vérifier les mises à jour…". Il est également toujours possible d'installer manuellement une nouvelle version de Zotero par-dessus votre version existante sans perdre de données.
