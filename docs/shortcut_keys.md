<p id="zotero-5-update-warning" style="color: red; font-weight: bold">We’re
in the process of updating the documentation for
<a href="https://www.zotero.org/blog/zotero-5-0">Zotero 5.0</a>. Some documentation
may be outdated in the meantime. Thanks for your understanding.</p>

# Raccourcis clavier

![/shortcut_pref.png](./images/shortcut_pref.png)

Cet onglet permet de modifier les raccourcis clavier par défaut de Zotero.

**Ouvrir/Fermer le Panneau Zotero** - Affiche/masque Zotero

**Basculer du mode plein écran** - Détermine si Zotero occupe la totalité de la fenêtre du navigateur, ou seulement une partie.

**Bibliothèque** - Met en valeur la bibliothèque Zotero.

**Recherche rapide** - Met en valeur la zone de recherche.

**Créer un nouvel élément** - Crée un nouvel item vierge dans la collection en cours.

**Créer une nouvelle Note** - Crée une nouvelle note indépendante dans la collection en cours.

**Bascule du sélecteur de marqueurs** - Affiche ou masque le sélecteur de marqueurs

**Copier les citations de l'élément sélectionné dans le Presse-Papiers** - Copie une citation de l'élément sélectionné dans le presse-papier. En fonction du style, elle peut être longue et détaillée, ou, si le style appelle des notes, un simple nombre.

**Copier les éléments sélectionnés dans le Presse-Papiers** - Copie la citation bibliographique complète dans le Presse-Papier.

**Essayer de gérer les conflits de raccourcis** - D'autres plugins Firefox peuvent essayer d'utiliser les mêmes raccourcis claviers que Zotero. Cette fonctionnalité peut vous éviter de devoir rechercher et désactiver le raccourci clavier dans l'autre plugin, en supposant que cela puisse même être envisagé (Zotero est évidemment bien plus important !).

Tous les changements apportés sur cette page ne prennent effet qu'à l'ouverture d'une nouvelle fenêtre du navigateur.

## Windows : raccourcis par défaut

<table>
<thead>
<tr class="header">
<th>Function</th>
<th>Command</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Ouvrir/Fermer le Panneau Zotero</td>
<td>Ctrl-Shift-Z</td>
</tr>
<tr class="even">
<td>Bascule du mode plein écran</td>
<td>Ctrl-Shift-F</td>
</tr>
<tr class="odd">
<td>Bibliothèque</td>
<td>Ctrl-Shift-L</td>
</tr>
<tr class="even">
<td>Recherche rapide</td>
<td>Ctrl-Shift-K</td>
</tr>
<tr class="odd">
<td>Créer un nouvel élément</td>
<td>Ctrl-Shift-N</td>
</tr>
<tr class="even">
<td>Créer une nouvelle note note</td>
<td>Ctrl-Shift-O</td>
</tr>
<tr class="odd">
<td>Bascule du sélecteur de marqueurs</td>
<td>Ctrl-Shift-T</td>
</tr>
<tr class="even">
<td>Copier les citations de l'élément Sélectionné dans le presse-papier</td>
<td>Ctrl-Shift-A</td>
</tr>
<tr class="odd">
<td>Copier les éléments sélectionnés dans le presse-papier</td>
<td>Ctrl-Shift-C</td>
</tr>
</tbody>
</table>

## MacOS : raccourcis par défaut

<table>
<thead>
<tr class="header">
<th>Function</th>
<th>Command</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Ouvrir/Fermer le Panneau Zotero</td>
<td>Cmd-Shift-Z</td>
</tr>
<tr class="even">
<td>Bascule du mode plein écran</td>
<td>Cmd-Shift-F</td>
</tr>
<tr class="odd">
<td>Bibliothèque</td>
<td>Cmd-Shift-L</td>
</tr>
<tr class="even">
<td>Recherche rapide</td>
<td>Cmd-Shift-K</td>
</tr>
<tr class="odd">
<td>Créer un nouvel élément</td>
<td>Cmd-Shift-N</td>
</tr>
<tr class="even">
<td>Créer une nouvelle note</td>
<td>Cmd-Shift-O</td>
</tr>
<tr class="odd">
<td>Bascule du sélecteur de marqueurs</td>
<td>Cmd-Shift-T</td>
</tr>
<tr class="even">
<td>Copier les citations de l'élément Sélectionné dans le presse-papier</td>
<td>Cmd-Shift-A</td>
</tr>
<tr class="odd">
<td>Copier les éléments sélectionnés dans le presse-papier</td>
<td>Cmd-Shift-C</td>
</tr>
</tbody>
</table>

![](tag>pref)
