# Préférences

#### La fenêtre des préférences

Beaucoup de fonctions de Zotero peuvent être personnalisées via la fenêtre des préférences de Zotero. Pour ouvrir les préférences, cliquez sur le menu "Édition -&gt; Préférences" (sur Windows ou Linux) ou "Zotero -&gt; Préférences" (sur Mac). Vous pouvez aussi utiliser le raccourci clavier `Ctrl/Cmd`-`,`.

![preferences_tabs.png](./images/preferences_tabs.png)

La fenêtre de préférences est divisée en différents panneaux.

-   **[Générales](/fr/preferences/general)** - Ajuster l'apparence, les paramètres d'importation et d'autres fonctions générales.
-   **[Synchronisation](/fr/preferences/sync)** - Configurer la synchronisation des données et des fichiers.
-   **[Recherche](/fr/preferences/search)** - Activer ou désactiver l’indexation du texte intégral des fichiers PDF et visualiser les statistiques correspondantes.
-   **[Exportation](/fr/preferences/export)** - Définir les paramètres par défaut pour la génération des bibliographies et des citations.
-   **[Citer](/fr/preferences/styles)** - Ajouter, retirer, modifier et prévisualiser les styles bibliographiques, et installer les modules de traitement de texte.
-   **[Avancées](/fr/preferences/advanced)** - Localisation des données Zotero, rechercher dans la bibliothèque, et autres paramètres avancés.

## Préférences cachées

En plus des préférences affichées dans la fenêtre des préférences de Zotero, il existe un certain nombre de [préférences cachées (page en anglais)](https://www.zotero.org/support/preferences/hidden_preferences) qui ne peuvent être modifiées que par le biais de l'Éditeur de configuration, accessible depuis l'onglet "Préférences -&gt; Avancées -&gt; Général".
