# Les styles bibliographiques

*Consulter cette page dans la documentation officielle de Zotero : [Citation Styles](https://www.zotero.org/support/styles) - dernière mise à jour de la traduction : 2022-06-08*

Zotero est livré avec plusieurs styles bibliographiques couramment utilisés pour [créer des citations et des bibliographies](./creating_bibliographies.md), et on trouve plus de 8100 styles supplémentaires dans [l'entrepôt des styles Zotero](https://www.zotero.org/styles). Tous ces styles sont écrits en langage CSL ([Citation Style Language](https://citationstyles.org/) ), un format également pris en charge par Mendeley, Papers, et bien d'autres applications.

## Installer des styles supplémentaires

### L'entrepôt des styles Zotero

Vous pouvez installer des styles à partir de [l'entrepôt des styles Zotero](https://www.zotero.org/styles) en cliquant sur le lien "Obtenir d'autres styles…" du [gestionnaire de styles Zotero](./cite.md) (dans le volet "Citer" des préférences de Zotero). Cherchez le style que vous souhaitez et cliquez sur l'intitulé du style pour l'installer dans Zotero. En utilisant un navigateur Firefox ou Chrome équipé du connecteur Zotero, vous pouvez aussi installer directement les styles dans Zotero à partir de la page web de [l'entrepôt des styles Zotero](https://www.zotero.org/styles).

L'entrepôt vous permet d'effectuer une recherche par intitulé de style, et de filtrer par type de style et par champ disciplinaire. En cochant la case "Show only unique styles", vous masquerez les styles dupliqués qui partagent exactement la même mise en forme (i.e. pour les styles propres à chaque revue de "Nature", "Nature Biotechnology", "Nature Chemistry", etc., seul le style *independent* "Nature" est affiché).

### Méthodes d'installation alternatives

Vous pouvez aussi installer des styles CSL (avec une extension “.csl”) à partir de fichiers enregistrés localement sur votre ordinateur (i. e. des styles que vous éditez vous-même ou que vous téléchargez à partir d'un autre site web). Dans le [gestionnaire de styles Zotero](./cite.md), cliquez sur le bouton '+', puis trouvez le fichier de style sur votre ordinateur.

## Gérer et modifier les styles

Vous pouvez retirer des styles installés en cliquant sur le bouton '-' dans le [gestionnaire de styles Zotero](./cite.md). A partir de cet onglet, vous pouvez aussi prévisualiser le résultat de la mise en forme effectuée par les styles sur des éléments sélectionnés de votre bibliothèque Zotero et [modifier des styles installés](https://www.zotero.org/support/dev/citation_styles).

## Signaler des erreurs de style

Si un style ne produit pas le résultat attendu, vérifiez tout d'abord que vous exécutez la dernière version (stable) de Zotero, et que vous avez bien la version la plus récente du style, installée à partir de [l'entrepôt des styles Zotero](https://www.zotero.org/styles). Une fois que vous vous êtes assuré que le style s'éloigne du guide du style, des instructions aux auteurs, ou d'exemples publiés, signalez l'erreur sur le [forum Zotero](https://forums.zotero.org). Pour votre billet, utilisez le titre "Style Error: \[Name of style\]", et fournissez une référence au guide du style (lien vers ou extrait de) qui montre que le style CSL n'est pas correct. Vous pouvez également [éditer le style](https://www.zotero.org/support/dev/citation_styles/style_editing_step-by-step) vous-même.

## Demandez de nouveaux styles

Si vous ne trouvez pas le style que vous cherchez dans [l'entrepôt des styles Zotero](https://www.zotero.org/styles), n'hésitez pas à [demander un style](https://github.com/citation-style-language/styles/wiki/Requesting-Styles). Lorsque vous demandez des styles, merci de fournir des références mises en forme pour l'article Campbell/Pedersen et le chapitre Mares mentionnés sur la page en lien. Merci de fournir aussi si possible un lien vers un article accessible gratuitement utilisant le style. Vous pouvez également [créer le style](https://www.zotero.org/support/dev/citation_styles) vous-même.

## Questions

Vous avez encore des interrogations? Consultez la base de connaissance, ou, si les pages de cette dernière n'apportent pas de réponse à votre question, recourez au [forum Zotero](https://forums.zotero.org/categories/).