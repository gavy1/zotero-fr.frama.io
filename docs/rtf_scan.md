# Analyse RTF

*Consulter cette page dans la documentation officielle de Zotero : [RTF Scan](https://www.zotero.org/support/rtf_scan) - dernière mise à jour de la traduction : 2022-06-08*

La fonction d'analyse RTF intégrée à Zotero permet aux utilisateurs de créer un document comportant des citations et une bibliographie sans jamais utiliser le [module pour traitement de texte](./word_processor_integration.md). Certains ont tout de suite en tête l'auteur et la date de la publication qu'ils veulent citer, et utiliser le module de traitement de texte peut alors les ralentir. Zotero peut cependant toujours mettre en forme les citations après coup.

Pour utiliser l'analyse RTF, créez un nouveau document au format RTF (Rich Text Format - format de texte enrichi) et commencez à écrire. Lorsque vous souhaitez ajouter une citation, écrivez-la normalement en respectant simplement l'un des formats suivants.

      {Smith, 2009}
      Smith {2009}
      {Smith et al., 2009}
      {John Smith, 2009}
      {Smith, 2009, 10-14}
      {Smith, "Title", 2009}
      {Jones, 2005; Smith, 2009}


Vous pouvez également installer dans Zotero [le style bibliographique RTF Scan](https://www.zotero.org/styles?q=id%3Artf-scan) et utiliser [la copie rapide](./creating_bibliographies.md#copie-rapide) pour copier facilement les citations au format attendu dans votre document, sans avoir à les taper. 

Si vous souhaitez que la bibliographie apparaisse à un autre emplacement qu'à la fin du document, tapez {Bibliography} à cet emplacement.

Une fois terminée l'écriture de votre document, enregistrez le fichier (assurez-vous qu'il est bien au format RTF) et ouvrez Zotero. Depuis le menu "Outils", choisissez "Analyse d'un fichier RTF". Sous "Fichier en lecture", choisissez le fichier que vous venez de créer. Sous "Fichier de sortie", spécifiez le nom et l'emplacement où vous souhaitez enregistrer le nouveau fichier mis en forme. Cliquez sur "Suivant".

![/rtf_scan.png](./images/rtf_scan.png)

L'écran de vérification des documents cités vous indiquera quelles citations ont été associées correctement et lesquelles restent ambiguës. Pour corriger une association erronée, cliquez sur l'icône à sa droite et sélectionnez la citation correcte dans la boîte de dialogue qui s'ouvre. Zotero vous fournira des suggestions pour les citations dont il n'est pas certain. Cliquer sur l'icône avec la flèche verte à droite de la suggestion l'associera à cette citation. Une fois que toutes les citations sont correctement associées, vous pouvez cliquer sur "Suivant".

À l'écran suivant, choisissez le style bibliographique que vous désirez utiliser et cliquez sur "Suivant". Zotero va alors créer votre document intégrant les citations et la bibliographie. Dans le fichier de sortie, toutes les citations seront mises en forme conformément au style choisi et, si le style le requiert, une bibliographie sera incluse à la fin du document, sauf si vous avez indiqué un autre emplacement.

Il est important de noter que si vous avez choisi un format de citation qui crée des notes de bas de page ou de fin, ces dernières apparaitront correctement uniquement si vous ouvrez le fichier de sortie dans un traitement de texte aux fonctionnalités complètes, comme Microsoft Word ou LibreOffice.

Le module complémentaire [RTF/ODF-Scan for Zotero](https://www.zotero.org/support/plugins#other_programs) fournit une version plus robuste de la fonction d'analyse RTF, en réduisant le risque de créer des citations ambiguës et en accordant plus de flexibilité pour inclure dans les citations des préfixes, des suffixes et des localisateurs (page, chapitre, verset, etc.). Ce module complémentaire nécessite LibreOffice pour être utilisé. 





