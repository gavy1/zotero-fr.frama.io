---
hide:
  - navigation
---
# Guide rapide pour démarrer
<style>
img.QSGimg {
  float: left;
  margin: 0 15px 10px 0;
  max-width: 180px;
}

@media (max-width: 450px) { 
    img.QSGimg {
        float: none;
        display: block;
        margin: 1em auto;
        max-width: ;
    }
 }

h2,h3{
  clear:left;
}

</style>


*Consulter cette page dans la documentation officielle de Zotero : [Quick start guide](https://www.zotero.org/support/quick_start_guide) - dernière mise à jour de la traduction : 2022-07-04*

## Qu'est-ce que Zotero ?

Zotero [zoh-TAIR-oh] est un logiciel de gestion bibliographique libre, gratuit et multiplateforme (Windows, Linux, Mac OS). Simple d'utilisation, il vous aide à collecter, organiser, citer et partager vos sources.

Lisez la suite pour un aperçu des fonctionnalités et possibilités de Zotero.

## Les fondamentaux

### Comment installer Zotero ?

Consultez les [instructions d'installation](./installation.md).

### Comment ouvrir Zotero?

![ouvrir Zotero](./images/how-to-open.png){class="QSGimg"}

Zotero s'exécute comme n'importe quel autre logiciel de votre ordinateur. 

### Que peut-on faire avec Zotero ?

![interface de Zotero](./images/intuitive_interface.png){class="QSGimg"}

Zotero est - dans son utilisation la plus basique - un logiciel de gestion bibliographique. Il est conçu pour stocker, gérer et citer des références bibliographiques, telles que des ouvrages et des articles. Dans Zotero, chacune de ces références constitue un "document". Plus largement, Zotero est un puissant outil pour collecter et organiser les sources et informations de votre recherche.

### La fenêtre principale

-   Le panneau de gauche liste les dossiers et sous-dossiers de votre bibliothèque de références. Vous disposez, par défaut, d'un dossier "Ma bibliothèque", vous pouvez y ajouter de nouveaux dossiers et sous-dossiers (collections).

-   Le panneau central correspond à la liste des références collectées dans Zotero. Vous avez ici un affichage dynamique en fonction du dossier (panneau de droite) dans lequel vous vous trouvez. Un tri des références est possible par auteurs, titres, dates, etc.

-   Le panneau à droite permet d'éditer et de modifier les données correspondant à une référence sélectionnée au niveau du panneau central.

![fenêtre principale](./images/fenetre_principale.png){class=imgcenter}

### Quels types de documents peuvent-ils être ajoutés dans Zotero ?

![types de référencess](./images/item_types.png){class="QSGimg"}

Chaque référence contient différentes données bibliographiques, en fonction du type de document dont il s'agit. Les références peuvent être de tout type, des ouvrages, articles et rapports, aux pages web, illustrations, films, lettres, manuscrits, enregistrements sonores, textes de lois, jugements, parmi bien d'autres.

### Que puis-je faire avec les références ?

![données bibliographiques](./images/item_metadata.png){class="QSGimg"}

Les références apparaissent dans le panneau central de l'interface de Zotero. Les données bibliographiques pour la référence sélectionnée apparaissent dans le panneau de droite. Elles comportent les titre, auteurs, maison d'édition, date, nombre de pages, et toute autre donnée nécessaire pour la citation du document.

## Organiser

### Les collections

![collections](./images/collections.png){class="QSGimg"}

Dans le panneau de gauche s'affiche "Ma bibliothèque", qui contient tous les documents de votre bibliothèque. Faites un clic-droit sur "Ma bibliothèque" ou cliquez sur le bouton "Nouvelle collection…" ![bouton Nouvelle collection](./images/add_collection.png) au-dessus du panneau gauche pour créer une nouvelle [collection](./collections_and_tags.md#les-collections), un dossier dans lequel les documents concernant un projet ou une thématique spécifique peuvent être placés. Considérez les collections comme des listes de lecture dans un lecteur de musique : dans les collections, les documents sont des alias (ou "liens") vers l'unique exemplaire de chaque document dans votre bibliothèque. Un même document peut appartenir à plusieurs collections à la fois.

### Les marqueurs

![marqueurs](./images/tags.png){class="QSGimg"}

Des [marqueurs (tags)](./collections_and_tags.md#les-marqueurs) peuvent être associés aux documents. Les intitulés des marqueurs sont choisis par l'utilisateur. À un document peuvent être attribués autant de marqueurs que nécessaire. Les marqueurs sont ajoutés ou supprimés avec le sélecteur de marqueurs en bas de la colonne de gauche ou, dans la colonne de droite, dans l'onglet "Marqueurs" de n'importe quel document. Jusqu'à 9 marqueurs peuvent se voir assigner une couleur. Les marqueurs colorés sont facilement visibles dans la liste des documents et peuvent être rapidement ajoutés ou retirés à l'aide des touches numériques de votre clavier. 

### Les recherches

![Recherches](./images/searches.png){class="QSGimg"}

La [recherche rapide](./searching.md#recherche-rapide) est directement accessible depuis la barre d'outils Zotero et affiche les documents dont les données bibliographiques, les marqueurs ou le contenu du texte intégral des pièces jointes correspondent aux termes saisis.
En cliquant sur l'icône loupe à gauche de la recherche rapide vous pouvez ouvrir la fenêtre [Recherche avancée](./searching.md#recherche-avancée), qui permet d'effectuer des recherches plus complexes ou plus précises.

### Les recherches enregistrées

![Recherches](./images/saved_searches.png){class="QSGimg"}

Les recherches avancées peuvent être enregistrées dans la colonne de gauche. Les [recherches enregistrées](./searching.md#recherches-enregistrées) fonctionnent comme les collections, mais sont automatiquement mises à jour avec les nouvelles références correspondant aux critères de recherche.

## Collecter

### Les fichiers joints

![Fichiers joints](./images/attachments.png){class="QSGimg"}

Les documents peuvent avoir des notes, des fichiers et des liens qui leur sont attachés. Ces [fichiers joints](./attaching_files.md) sont visibles dans le panneau central, sous leur document parent. Les fichiers joints peuvent être affichés ou masqués en cliquant sur la flèche à côté de leur document parent.

### Les notes

![Notes](./images/notes.png){class="QSGimg"}

Des [notes en texte enrichi](./notes.md) peuvent être attachées à n'importe quel document en utilisant l'onglet "Notes" dans le panneau de droite. Elles peuvent être modifiées dans le panneau de droite ou dans une fenêtre dédiée. Cliquez sur le bouton "Nouvelle note" ( ![Bouton Nouvelle note](./images/note_add.png) ) dans la barre d'outils pour créer une note sans l'attacher à un document.

### Les fichiers

![Fichiers](./images/files.png){class="QSGimg"}

N'importe quel type de fichier peut être [attaché](./attaching_files.md) à une référence. Il est possible d'ajouter des fichiers avec le bouton "Ajouter une pièce jointe" ( ![Bouton Ajouter une pièce jointe](./images/add_file_button.png) ), en faisant un clic-droit sur une document existant ou en faisant un glisser-déposer. Les fichiers n'ont pas besoin d'être attachés à un document existant. Ils peuvent simplement être ajoutés à votre bibliothèque. Les fichiers peuvent aussi être téléchargés automatiquement quand vous collectez des références en utilisant le [connecteur Zotero de votre navigateur](#collecter-une-référence).

### Les liens et captures de pages web

![Liens et snapshots](./images/snapshots_links.png){class="QSGimg"}

[Des pages web](./attaching_files.md#captures-de-pages-web) peuvent être attachées à n'importe quel document comme lien ou snapshot (instantané). Un lien ouvre juste la page web en ligne. Zotero peut aussi capturer une page web pour enregistrer un snapshot. Un snapshot est une copie d'une page web stockée localement dans le même état qu'au moment de son enregistrement. Les snapshots peuvent être visionnés sans connexion Internet.

### Collecter des documents

![Bouton de capture](./images/capture.png){class="QSGimg"}

Avec le connecteur Zotero pour Chrome, Firefox ou Safari, il est facile de créer de nouveaux documents à partir d'informations disponibles sur Internet. En un seul clic, Zotero peut automatiquement créer une référence du type approprié, compléter les données bibliographiques, télécharger s'il est disponible le fichier PDF de texte intégral et ajouter les liens utiles (par exemple le lien vers une notice PubMed) ou des fichiers de données supplémentaires.

### Collecter un ou plusieurs documents

![Collecte multiple](./images/multiple_capture.png){class="QSGimg"}

Si l'icône de [capture](./adding_items_to_zotero.md) est un livre, un article, une image, etc., cliquer sur cette icône ajoutera le document à la collection en cours de sélection dans Zotero. Si l'icône de capture est un dossier, la page Web contient plusieurs documents. En cliquant sur l'icône, vous ouvrez une boîte de dialogue à partir de laquelle les documents peuvent être sélectionnés et enregistrés dans Zotero.

### Convertisseurs

![Traducteurs](./images/save_button_book.png){class="QSGimg"}

Zotero utilise des morceaux de code appelés [convertisseurs](https://www.zotero.org/translators) pour reconnaître les informations sur les pages Web. Il existe des convertisseurs génériques qui fonctionnent avec de nombreux sites, et des convertisseurs écrits pour des sites individuels. Si un site que vous utilisez ne dispose pas d'un convertisseur, n'hésitez pas à en demander un sur les [forums de Zotero](https://www.zotero.org/forum). 

### Enregistrer une page web

![Enregistrer une page web](./images/save_button_webpage.png){class="QSGimg"}

Si le connecteur Zotero ne reconnaît pas les informations présentes sur une page web, vous pouvez tout de même cliquer sur le bouton de capture dans la barre d'outils de votre navigateur pour enregistrer la page en tant que document de type [Page Web](./attaching_files.md#captures-de-pages-web) avec un snapshot attaché. Bien que cela enregistre les métadonnées basiques (titre, URL, date de consultation), il sera éventuellement nécessaire d'ajouter manuellement d'autres informations à la référence.

### Ajouter un document par son identifiant

![Ajout par identifiant.png](./images/identifier.png){class="QSGimg"}

Zotero peut [ajouter des documents automatiquement](./adding_items_to_zotero.md#ajouter-un-document-par-son-identifiant) en utilisant leur numéro ISBN (International Standard Book Number), leur DOI (Digital Object Identifier), leur identifiant PubMed, arXiv ou leur bibcode ADS. Cliquez sur "Ajouter un document par son identifiant" dans la barre d'outils Zotero, saisissez l'identifiant et cliquez sur OK. Il est également possible de copier-coller ou de saisir (appuyez sur `Maj+Entrée` pour obtenir un champ de saisie plus grand) une liste d'identifiants en une seule fois.

### Les flux RSS

![Flux RSS](./images/feeds.png){class="QSGimg"}

Abonnez-vous aux [flux RSS](./feeds.md) de vos revues ou sites web préférés pour vous tenir au courant des dernières recherches. Accédez à la page web de l'article ou enregistrez les articles dans votre bibliothèque en cliquant sur un bouton. 

### Ajouter manuellement des documents

![Ajout manuel](./images/add_item.png){class="QSGimg"}

Les documents peuvent être [ajoutés manuellement](./adding_items_to_zotero.md#ajouter-manuellement-des-documents) en cliquant sur ​​le bouton "Nouveau document" (![Nouveau document](./images/add.png)) dans la barre d'outils de Zotero, puis en sélectionnant le type de document approprié. Les métadonnées peuvent ensuite être ajoutées manuellement dans la colonne de droite. Si d'une façon générale vous devez éviter la saisie manuelle, cette option est particulièrement adaptée pour l'ajout de sources primaires qui ne sont pas disponibles en ligne.


## Citer

### Citer des documents

![Styles de citation](./images/styles.png){class="QSGimg"}

Zotero utilise le langage style CSL (Citation Style Language) pour mettre en forme correctement les citations, selon différents styles bibliographiques. Zotero prend en charge les principaux [styles](./styles.md) (Chicago, MLA, APA, Vancouver, etc) ainsi que de très nombreux styles spécifiques.

### L'intégration aux logiciels de traitements de texte

![Intégration aux traitements de texte](./images/word_integration_tab.png){class="QSGimg"}

Les [modules complémentaires de Zotero pour Word, LibreOffice et Google Docs](./word_processor_integration.md) permettent d'insérer des citations de sa bibliothèque directement dans son traitement de texte. Cela permet de citer plusieurs pages ou sources ou de personnaliser les citations en un tournemain. Les citations dans le texte, les notes de bas de page et les notes de fin sont toutes prises en charge. Grâce à des [modules complémentaires](https://www.zotero.org/support/plugins#latex_tex_and_plain_text_editors) développés par la communauté, Zotero peut également être utilisé avec LaTeX, Scrivener et de nombreux autres logiciels de traitement de texte. 

### Bibliographies automatiques

![Bibliographies automatiques](./images/bibliography.png){class="QSGimg"}

En utilisant les [modules complémentaires pour traitements de texte](/word_processor_integration.md), il est possible de générer automatiquement une bibliographie à partir des documents cités et de modifier le style bibliographique de l'ensemble d'un document en cliquant juste sur un bouton.

### Bibliographies manuelles

![Bibliographies manuelles](./images/manual_bib.png){class="QSGimg"}

Zotero peut aussi insérer [des citations et des bibliographies](./creating_bibliographies.md) dans n'importe quel champ de texte ou logiciel. Il vous suffit de glisser-déposer les documents, d'utiliser la "Copie rapide" pour exporter les citations dans le presse-papier, ou de les exporter dans un fichier.

## Collaborer

### La synchronisation

![Synchronisation](./images/sync.png){class="QSGimg"}

Vous pouvez utiliser Zotero sur plusieurs ordinateurs grâce à la [synchronisation Zotero](./sync.md). Les références de la bibliothèque et les notes sont synchronisées en utilisant les serveurs de Zotero (stockage illimité), tandis que les pièces jointes peuvent utiliser les serveurs de Zotero ou votre propre service WebDAV pour synchroniser les fichiers tels que les PDF, images, audio/vidéo.

### Les serveurs de Zotero

![En ligne](./images/online_mylibrary.png){class="QSGimg"}

Les [références synchronisées](./sync.md) sur le serveur Zotero peuvent être consultées en ligne depuis votre compte [zotero.org](https://www.zotero.org/). Vous pouvez partager votre bibliothèque avec d'autres personnes ou créer un CV personnalisé en utilisant des références sélectionnées.

Mettez des copies de vos recherches à la disposition des lecteurs, du public et d'autres chercheurs sur [zotero.org](https://www.zotero.org/) en utilisant [Mes publications](./my_publications.md).

### Les groupes

![Groupes](./images/groups.png){class="QSGimg"}

Les utilisateurs de Zotero peuvent créer des [groupes](./groups.md) collaboratifs ou d'intérêt. Les bibliothèques de groupe partagées permettent de gérer de manière collaborative les sources et le matériau de recherche, à la fois en ligne et via le client Zotero. [Zotero.org](https://www.zotero.org/) peut être le cœur de la recherche, de la communication et de l'organisation de votre groupe de travail. 
