<p id="zotero-5-update-warning" style="color: red; font-weight: bold">We’re
in the process of updating the documentation for
<a href="https://www.zotero.org/blog/zotero-5-0">Zotero 5.0</a>. Some documentation
may be outdated in the meantime. Thanks for your understanding.</p>

### Comment puis-je mettre en forme certains mots d'un titre: par ex. en italique, en exposant ou en indice ?

Pour utiliser ponctuellement la mise en forme enrichie, il faut ajouter manuellement les marqueurs suivants aux champs de votre bibliothèque Zotero:

-   &lt;i&gt; et &lt;/i&gt; pour *l'italique*
-   &lt;b&gt; et &lt;/b&gt; pour **le gras**
-   &lt;sub&gt; et &lt;/sub&gt; pour <sub>l'indice</sub>
-   &lt;sup&gt; et &lt;/sup&gt; pour <sup>l'exposant</sup>
-   &lt;span style="font-variant:small-caps;"&gt; and &lt;/span&gt; pour <span style="font-variant: small-caps;">les petites majuscules</span>

Zotero remplacera automatiquement ces marqueurs par la bonne mise en forme, par exemple "&lt;i&gt;Escherichia coli&lt;/i&gt; et les β-lactamases à spectre étendu" s'affichera dans vos notes et bibliographie ainsi: "*Escherichia coli* et les β-lactamases à spectre étendu".  
Autre exemple: "La fluidité de l'eau dans nos cellules : H&lt;sub&gt;2&lt;/sub&gt;O, la molécule du vivant" s'affichera: "La fluidité de l'eau dans nos cellules : H<sub>2</sub>O, la molécule du vivant".

Attention: si une mise en forme doit s'appliquer à l'intégralité du texte d'un champ (parce qu'une revue exige, par exemple, que les titres apparaissent en italiques), il faut alors modifier le style de citation CSL directement (consultez la [documentation sur les styles CSL](https://www.zotero.org/support/dev/citation_styles) et les [options CSL de mise en forme des champs](http://citationstyles.org/downloads/specification.html#formatting)). Il serait peu efficace (et absurde) d'ajouter à tous vos champs le marqueur &lt;i&gt;&lt;/i&gt;!

![](tag>kb-fr styles-fr)
