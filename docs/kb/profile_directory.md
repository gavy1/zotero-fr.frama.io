# Emplacement du répertoire de profil

## Zotero

Vous pouvez trouver votre répertoire de profil Zotero à l'emplacement suivant.

<table>
<thead>
<tr class="header">
<th>Mac</th>
<th><code>/Users/&lt;username&gt;/Library/Application Support/Zotero/Profiles/&lt;chaîne_aléatoire&gt;</code> <small>Note: Le répertoire /Users/&lt;username&gt;/Library est masqué par défaut. Pour y accéder, cliquez sur votre bureau, maintenez la touche Option enfoncée, cliquez sur le menu Finder's Go, puis sélectionnez Bibliothèque dans le menu.</small></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Windows 10/8/7/Vista</td>
<td><code>C:\Users\&lt;User Name&gt;\AppData\Roaming\Zotero\Zotero\Profiles\&lt;chaîne_aléatoire&gt;</code> <small>Note:Si AppData est masqué sur votre système, cliquez sur la barre de recherche (ou Démarrer avant Windows 10), tapez <code>%appdata%</code>, puis appuyez sur Entrée, ce qui devrait vous amener au répertoire AppData\Roaming. Vous pouvez alors ouvrir le reste du chemin.
</small></td>
</tr>
<tr class="even">
<td>Windows XP/2000</td>
<td><code>C:\Documents and Settings\&lt;username&gt;\Application Data\Zotero\Zotero\Profiles\&lt;chaîne_aléatoire&gt;</code></td>
</tr>
<tr class="odd">
<td>Linux</td>
<td><code>~/.zotero/zotero/&lt;chaîne_aléatoire&gt;</code></td>
</tr>
</tbody>
</table>

#### Zotero 4.0 for Firefox

Suivez [ces instructions](http://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data?redirectlocale=en-US&redirectslug=Profiles) pour localiser votre répertoire de profil.

![](tag>kb-fr)
