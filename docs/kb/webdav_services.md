# Liste de services WebDAV

Cette page contient une liste de services WebDAV qui proposent une offre gratuite et que des utilisateurs ont déclaré avoir utilisé avec succès avec Zotero. Les offres gratuites peuvent avoir certaines limitations. Tous les fournisseurs de services proposent également des offres payantes plus importantes et moins limitées. La liste n'est pas exhaustive. Des fournisseurs de services qui ne figurent pas dans cette liste peuvent néanmoins fonctionner avec Zotero.

**Cette liste est générée par les utilisateurs et n'implique aucune approbation d'aucun des services par Zotero. Zotero fonctionne avec des serveurs WebDAV correctement spécifiés et ne peut fournir qu'une assistance minimale en cas de problèmes avec des fournisseurs WebDAV tiers.**

<table>
<thead>
<tr class="header">
<th>Service</th>
<th>Espace gratuit</th>
<th>URL WebDAV</th>
<th>Remarques et limitations</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="http://www.4shared.com/features.jsp">4shared</a></td>
<td>15 Go</td>
<td>https://webdav.4shared.com/zotero</td>
<td>(non officiel) Taille maximale de fichier : 2 Go. <a href="/forum/discussion/4559/2/which-webdav-service-for-zotero/#Item_23">Nombre maximum de fichiers par dossier : 5000 (≈2,500 pièces jointes Zotero)</a>.</td>
</tr>
<tr class="even">
<td><a href="https://www.cloudme.com/en/pricing">CloudMe</a></td>
<td>3 Go</td>
<td>https://webdav.cloudme.com/{username}/xios/zotero</td>
<td>Taille maximale de fichier : 150 Mo. <a href="/forum/discussion/75169/webdav-error-for-a-delete-request-with-cloudme-id-1840279011">Des utilisateurs ont rapporté des erreurs de synchronisation avec le WebDAV CloudMe</a></td>
</tr>
<tr class="odd">
<td><a href="https://www.driveonweb.de/fuer-privatanwender/produktbeschreibung">DriveOnWeb</a></td>
<td>3 Go</td>
<td>https://storage.driveonweb.de/probdav/zotero</td>
<td>La documentation est en allemand.</td>
</tr>
<tr class="even">
<td><a href="https://drive.google.com">Google Drive</a></td>
<td>3 Go</td>
<td>Nécessite de mettre en place un <a href="https://github.com/mikea/gdrive-webdav">pont WebDAV</a> ou d'utiliser un service tiers pour fournir un accès WebDAV à Google Drive, dans la mesure où <a href="https://dav-pocket.appspot.com/webdav_access_to_google_docs">DAV-Pocket</a> (<a href="/forum/discussion/68527/dav-pocket-and-google-drive">pourrait ne plus fonctionner</a>). Les URL WebDAV varient en fonction des services. Certains services ne fonctionneront pas sur tous les systèmes d'exploitation.</td>
<td><a href="/sync#alternative_syncing_solutions">Envisagez d'utiliser des fichiers liés avec Zotfile au lieu de la synchronisation WebDAV avec Google Drive</a></td>
</tr>
<tr class="odd">
<td><a href="https://www.free-hidrive.com/product/hidrive-free.html">HiDrive</a></td>
<td>5 Go</td>
<td>https://webdav.hidrive.strato.com/users/{username}/zotero</td>
<td></td>
</tr>
<tr class="even">
<td><a href="https://www.idrivesync.com/pricing">iDriveSync</a></td>
<td>10 Go</td>
<td>https://dav.idrivesync.com/zotero</td>
<td><a href="/forum/discussion/26858/webdav-syncing-with-idrivesync-is-working/">Certains utilisateurs ont rencontré des des difficultés avec ce service</a> et la documentation <a href="https://www.idrivesync.com/webdav">recommande de ne pas l'utiliser à grande échelle</a>.</td>
</tr>
<tr class="odd">
<td><a href="https://koofr.eu/blog/posts/koofr-with-zotero-via-webdav">Koofr</a></td>
<td>10 Go</td>
<td>https://app.koofr.net/dav/Koofr/zotero</td>
<td>Consultez les instructions de paramétrage <a href="https://koofr.eu/blog/posts/koofr-with-zotero-via-webdav">ici</a>.</td>
</tr>
<tr class="even">
<td><a href="https://www.pcloud.com">pCloud</a></td>
<td>10 Go</td>
<td>https://webdav.pcloud.com:443/zotero</td>
<td>Taille de fichier et vitesse de transfert illimitées. Un stockage supplémentaire gratuit de 10 Go est disponible par le biais de recommandations.</td>
</tr>
<tr class="odd">
<td><a href="http://www.storegate.com/en/home-user-usd/plans/">Storegate</a></td>
<td>2 Go</td>
<td>https://webdav1.storegate.com/{username}/home/{username}/zotero</td>
<td></td>
</tr>
<tr class="even">
<td><a href="https://disk.yandex.ru">Yandex Disk</a></td>
<td>10 Go</td>
<td>https://webdav.yandex.ru/zotero</td>
<td>La documentation est en russe.</td>
</tr>
</tbody>
</table>

Note: La partie "/zotero" à la fin de l'URL WebDAV est ajoutée automatiquement par Zotero et ne doit pas être incluse lors de la saisie de l'URL WebDAV dans les préférences de Zotero.

![](tag>kb-fr sync-fr)
