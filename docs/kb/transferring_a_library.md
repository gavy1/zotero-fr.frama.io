#### Comment puis-je transférer ma bibliothèque Zotero vers un autre ordinateur ?

##### Option A: Copier le répertoire de données Zotero

La façon la plus fiable de transférer entièrement votre bibliothèque Zotero vers un autre ordinateur est de copier le [répertoire des données Zotero](../zotero_data.md) depuis votre premier ordinateur vers votre nouvel ordinateur.

Pour localiser vos données Zotero, ouvrez les [préférences](./preferences.md) de Zotero et cliquez sur "Ouvrir le répertoire de données" dans l'onglet Avancées -&gt; Fichiers et dossiers. Reportez-vous à [la rubrique "Emplacements par défaut"](../zotero_data.md#emplacements_par_defaut.md) pour connaître les emplacements par défaut du répertoire de données.

Assurez-vous d'avoir fermé Zotero sur les deux ordinateurs avant de copier les fichiers Zotero.

En plus de la méthode ci-dessus, vous pouvez aussi [utiliser une clé USB pour synchroniser vos données entre plusieurs ordinateurs](.https://www.zotero.org/support/kb/using_multiple_computers).

##### Option B: Activer la synchronisation Zotero

Vous pouvez également [synchroniser](../sync.md) automatiquement votre bibliothèque Zotero entre plusieurs ordinateurs via les serveurs Zotero.

##### Avertissement

Exporter et importer votre bibliothèque (par exemple via Zotero RDF) n'est pas une option recommandée. Aucun des formats d'export disponibles ne permet un transfert complet et sans perte des données de votre bibliothèque. De plus cela brisera les connexions existantes entre votre bibliothèque Zotero et vos fichiers de traitement de texte.

![](tag>kb-fr)
