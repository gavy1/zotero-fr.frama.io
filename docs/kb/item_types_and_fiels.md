# Les types de documents et les champs associés dans Zotero

*Pour faciliter la mise à jour de la traduction et la mise en correspondance des termes en français avec les termes originaux en anglais, l'ordre des lignes dans les tableaux suit celui de la version originale en anglais de cette page.*

Cette page décrit les types de documents Zotero et les champs associés. Ces informations peuvent être utiles pour ajouter manuellement des documents dans Zotero ou sur la manière de remplir les champs pour les types de documents moins communs.

## Types de documents

Dans Zotero, les types de documents doivent être considérés comme des catégories larges et flexibles. Généralement, il convient de choisir le type de document d'après la manière dont ce document doit être référencé (voir la description des types de document Zotero ci-dessous). Pour des documents inhabituels, qui ne correspondent pas parfaitement à l'une des catégories prises en charge, on peut choisir un type de documents suffisamment proche, proposant un ensemble approprié de champs (`Livre`, `Rapport` ou `Manuscrit` peuvent notamment être utilisés de manière satisfaisante pour la plupart des types inhabituels de documents).

Plus d'informations spécifiques sur un type de document (par exemple, un roman par opposition à un livre biographique) peuvent être indiquées dans les champs Type, Format ou Extra, ou en utilisant les [marqueurs](../collections_and_tags.md#marqueurs).

<table>
<thead>
<tr class="header">
<th>Type de document</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Illustration</td>
<td>Une œuvre d'art (p. ex. une peinture à l'huile, une photographie ou une sculpture). Utilisez également ce type de document pour d'autres types d'images ou de documents visuels (p. ex. un graphique ou une figure dans une publication scientifique).</td>
</tr>
<tr class="even">
<td>Enregistrement audio</td>
<td>Toute forme d'enregistrement audio, y compris de la musique, des paroles prononcées, des effets sonores, des archives sonores ou des données scientifiques audio.</td>
</tr>
<tr class="odd">
<td>Projet/proposition de loi</td>
<td>Une législation projetée.</td>
</tr>
<tr class="even">
<td>Billet de blog</td>
<td>Un article ou un billet publié sur un site personnel de blog. Pour des articles en ligne publiés dans le cadre d'une publication en ligne de plus grande envergure (p. ex. <a href="http://www.nytimes.com/interactive/blogs/directory.html">les blogs du New York Times</a>), les types de documents <code>Article de magazine</code> ou <code>Article de journal</code> permettent généralement d'obtenir de meilleurs résultats.</td>
</tr>
<tr class="odd">
<td>Livre</td>
<td>Un livre ou un document semblable, publié. Pour les documents gouvernementaux, les rapports techniques, les manuels d'utilisation, etc., utilisez le type <code>Rapport</code>. Le type <code>Livre</code> peut également être adapté pour référencer des documents inhabituels.</td>
</tr>
<tr class="even">
<td>Chapitre de livre</td>
<td>Une section d'un livre, tel que chapitres, avant-propos, préfaces, introductions, annexes, postfaces, etc.</td>
</tr>
<tr class="odd">
<td>Affaire</td>
<td>Une affaire juridique (une cause, un procès), publiée ou non.</td>
</tr>
<tr class="even">
<td>Article de colloque</td>
<td>Un article présenté lors d'un colloque et publié par la suite dans des actes (sous la firme d'un livre, d'un rapport ou du numéro d'un journal). Pour les articles présentés lors d'un colloque mais qui n'ont pas été publiés par la suite, utilisez le type <code>Présentation</code>.</td>
</tr>
<tr class="odd">
<td>Article de dictionnaire</td>
<td>Un article publié comme partie d'un dictionnaire.</td>
</tr>
<tr class="even">
<td>Document</td>
<td>Un type générique de document. Ce type offre un choix restreint de champs et les styles de citation le prennent mal en charge. Il vaut généralement mieux éviter de s'en servir.</td>
</tr>
<tr class="odd">
<td>Courriel</td>
<td>Un message envoyé par courrier électronique. Ce type de document peut aussi être utilisé pour d'autres formes de communication personnelle.</td>
</tr>
<tr class="even">
<td>Article d'encyclopédie</td>
<td>Un article ou un chapitre publié dans une encyclopédie.</td>
</tr>
<tr class="odd">
<td>Film</td>
<td>Un film. Utilisez généralement ce type pour des films publiés, fictionnels, artistiques ou documentaires. Pour d'autres types de documents vidéos, utilisez <code>Enregistrement vidéo</code>.</td>
</tr>
<tr class="even">
<td>Message de forum</td>
<td>Un message sur un forum de discussion en ligne. Utilisez également ce type pour des documents comme des messages Facebook ou des tweets.</td>
</tr>
<tr class="odd">
<td>Audience</td>
<td>Une audience formelle ou un rapport de réunion d'un corps législatif.</td>
</tr>
<tr class="even">
<td>Message instantané</td>
<td>Un message envoyé par le biais d'une messagerie instantanée ou d'un service de chat. Ce type peut aussi être utilisé pour d'autres formes de communication personnelle.</td>
</tr>
<tr class="odd">
<td>Interview</td>
<td>Un entretien avec une personne, y compris enregistré, transcrit ou dont une trace est conservé sous une autre forme.</td>
</tr>
<tr class="even">
<td>Article de revue</td>
<td>Un article publié dans une revue académique (imprimée ou en ligne).</td>
</tr>
<tr class="odd">
<td>Lettre</td>
<td>Une lettre envoyée entre des personnes ou des organisations. Ce type peut aussi être utilisé pour d'autres formes de communication personnelle.</td>
</tr>
<tr class="even">
<td>Article de magazine</td>
<td>Un article publié dans une revue non-académique, populaire, professionnelle ou spécialisée (imprimée ou en ligne).</td>
</tr>
<tr class="odd">
<td>Manuscrit</td>
<td>Un manuscrit non publié. Utilisez ce type tant pour des documents historiques que pour des travaux modernes non publiés (par exemples des manuscrits soumis à un éditeur ou des "working papers" qui ne font pas l'objet d'une large diffusion). Ce type peut également être utilisé pour d'autres formes de documents historiques ou d'archives, ou être adapté pour référencer des documents inhabituels.</td>
</tr>
<tr class="even">
<td>Carte</td>
<td>Une carte. Utilisez également ce type pour des modèles géographiques.</td>
</tr>
<tr class="odd">
<td>Article de journal</td>
<td>Un article publié dans un journal (presse quotidienne), imprimé ou en ligne.</td>
</tr>
<tr class="even">
<td>Brevet</td>
<td>Un brevet délivré pour une invention technique ou pour un design.</td>
</tr>
<tr class="odd">
<td>Balado (Podcast)</td>
<td>Un épisode de <a href="https://fr.wikipedia.org/wiki/Podcasting">podcast</a> (un programme audio ou vidéo diffusé en ligne, souvent sur abonnement).</td>
</tr>
<tr class="even">
<td>Présentation</td>
<td>Une présentation faite lors d'un colloque, congrès, symposium, conférence, etc. Ce type se réfère à la présentation elle-même, et non à la version écrite publiée dans les actes du colloque (pour de telles versions publiées, utilisez le type <code>Article de colloque</code>).</td>
</tr>
<tr class="odd">
<td>Émission de radio</td>
<td>Une émission de radio, comme une émission d'actualité ou un épisode d'une série radiophonique. Peut concerner aussi des émissions mises en ligne ou produites pour Internet (cf. <code>Podcast</code>).</td>
</tr>
<tr class="even">
<td>Rapport</td>
<td>Un rapport publié par une organisation, une institution, un département gouvernement ou une entité similaire. Ce type peut être utilisé aussi pour les «working papers» et les «preprints» diffusés sur des dépôts institutionnels. Ce type peut servir aussi à référencer des documents inhabituels.</td>
</tr>
<tr class="odd">
<td>Programme informatique</td>
<td>Un logiciel, une application ou tout autre programme informatique.</td>
</tr>
<tr class="even">
<td>Acte juridique</td>
<td>Une loi ou tout autre document à caractère législatif entré en vigueur.</td>
</tr>
<tr class="odd">
<td>Thèse</td>
<td>Une thèse ou mémoire présenté par un étudiant en vue de l'obtention d'un titre (Master, doctorat, etc), publié ou non.</td>
</tr>
<tr class="even">
<td>Émission de TV</td>
<td>Une émission télévisée, par exemple un épisode de série.</td>
</tr>
<tr class="odd">
<td>Enregistrement vidéo</td>
<td>Un enregistrement vidéo. Ce type est destiné aux documents vidéo génériques qui ne correspondent pas à un des types plus spécifiques (p. ex. <code>Film</code> ou <code>Émission de TV</code>). Il s'agit par exemple de vidéos YouTube ou de vidéos scientifiques.</td>
</tr>
<tr class="even">
<td>Page Web</td>
<td>Une page d'un site web. Si possible, il vaut mieux utiliser un des types plus spécifiques (p. ex. <code>Article de magazine</code>, <code>Billet de blog</code> ou <code>Rapport</code>).</td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Pièce jointe</td>
<td>Un fichier joint indépendant (p. ex. un fichier PDF, JPEG, DOCX, PPTX, XLSX ou ODT). Zotero offre des fonctionnalités limitées concernant ces fichiers indépendants (il n'est p. ex. pas possible de faire des recherches dans leur contenu). Il vaut mieux <a href="/fr/attaching_files#fichiers_enfants_et_fichiers_independants">rattacher les fichiers à de véritables documents Zotero</a>.</td>
</tr>
<tr class="odd">
<td>Note</td>
<td>Une note indépendante. Des notes peuvent être utilisées pour organiser et annoter dans Zotero. Si vous citez une note indépendante, Zotero en utilisera les 120 premiers signes comme titre (et considérera que la note n'a ni auteur ni date). Citer des notes n'est toutefois pas une manière fiable d'ajouter des commentaires à une bibliographie / liste de références.</td>
</tr>
</tbody>
</table>

Pour davantage de types de documents juridiques et historiques, se référer à [Legal Citations](https://www.zotero.org/support/kb/legal_citations).

## Champs associés dans Zotero

Les champs disponibles pour divers types de documents dans Zotero sont décrits ci-dessous. Les champs sont organisés en catégories en fonction des types de documents auxquels ils sont le plus souvent associés. Certains champs (par exemple Volume, Collection) peuvent également être présents pour d'autres types de documents. Lorsque cela est nécessaire, les significations spécifiques d'un champ pour des types de documents déterminés sont indiquées.

Les champs marqués d'un astérisque (\*) ne peuvent pas être utilisés dans les citations.

### Champs généraux

Les champs suivants existent pour plusieurs types de documents et ont la même destination pour tous (ou presque tous) les types de documents.

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Type de documents</td>
<td>Le type de document général. Les types de document sont généralement déterminés sur la base de la façon dont ils devraient être cités. Reportez-vous aux descriptions des types de document de Zotero listés ci-dessus. Vous pouvez stocker plus d'informations spécifiques sur le type de document (p.ex. un roman par rapport à une biographie pour un livre) dans les champs Type de document, Format et Extra ou en utilisant les <a href="https://www.zotero.org/support/collections_and_tags#tags">marqueurs</a>.</td>
</tr>
<tr class="even">
<td>Créateurs</td>
<td>Les personnes ou les institutions créant ou associées à la création de l’œuvre. Un document peut avoir plusieurs créateurs de différents types. Reportez-vous à la liste des créateurs ci-dessous.</td>
</tr>
<tr class="odd">
<td>Titre</td>
<td>Le titre principal du document. Devrait être entré en <a href="https://www.zotero.org/support/fr/kb/sentence_casing">"sentence case" [sans capitale</a>].</td>
</tr>
<tr class="even">
<td>Résumé</td>
<td>Courte description de l’œuvre</td>
</tr>
<tr class="odd">
<td>Date</td>
<td>Date de publication. Utilisez "Consultez le" pour la date de consultation des ressources électroniques.</td>
</tr>
<tr class="even">
<td>Titre abrégé</td>
<td>Forme courte du titre, souvent sans le sous-titre. Surtout utilisé dans les styles Note pour les citations d'une œuvre déjà citée.</td>
</tr>
<tr class="odd">
<td>Langue</td>
<td>Langue de publication du document. Nous recommandons de la stocker selon le format <a href="https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1">codes de langue ISO</a> à deux lettres suivi du <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">code du pays</a> à 2 lettres (ex. en-US pour anglais américain ou de-DE pour l'allemand). Le champ Langue est utilisé pour déterminer si <a href="https://www.zotero.org/support/fr/kb/preventing_title_casing_for_non-english_titles">tous les mots du titre doivent commencer par une lettre capitale</a>. Si les données du document stocké sont en anglais (p.ex. pour les titres traduits), entrez en-US ou en-UK, même si le contenu du document est dans une autre langue.</td>
</tr>
</tbody>
</table>

### Champs pour les livres et les périodiques

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Publication</td>
<td>Titre du périodique (revue, magazine,journal, etc.) contenant le document cité</td>
</tr>
<tr class="even">
<td>Titre du livre<br />
Titre du dictionnaire<br />
Titre de l'encyclopédie</td>
<td>Titre du livre, du dictionnaire, de l'encyclopédie dont le chapitre ou l'entrée a été publié</td>
</tr>
<tr class="odd">
<td>Volume</td>
<td>Le volume (dans une publication en plusieurs volumes) dans lequel un article ou un chapitre a été publié. Pour les volumes numériques, entrez seulement le chiffre. Si le titre du volume doit être cité, insérez le titre dans ce champ (ex. "2: Organizational Psychology").</td>
</tr>
<tr class="even">
<td>Numéro</td>
<td>Le numéro d'un périodique. En général, les numéros commencent à 1 au début de chaque année/volume. Si le périodique n'a qu'une numéro de volume, laissez ce champ vide.</td>
</tr>
<tr class="odd">
<td>Pages</td>
<td>Les numéros de pages du document dans une publication plus grande. Peut aussi être utilisé pour les localisateurs ou numéros d'article dans les journaux et livres électroniques.</td>
</tr>
<tr class="even">
<td>Édition</td>
<td>L'édition de l’œuvre. Si numérique, entrez simplement le chiffre arabe (p.ex. "2" au lieu "deuxième").</td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Collection</td>
<td>Titre de la collection contenant plusieurs publications ou présentations (ex., "Cambridge Studies in Comparative Politics", "Special issue on Advances in Personality Assessment")</td>
</tr>
<tr class="odd">
<td>N° ds la coll.</td>
<td>Numéro d'un document dans une collection</td>
</tr>
<tr class="even">
<td>Titre de la coll.</td>
<td>Titre d'une série d'article <em>au sein</em> d'un numéro d'une revue (ex. un dossier spécial ou "From the Cover"). "Titre de section" serait une dénomination plus appropriée. Voir <a href="https://dtd.nlm.nih.gov/publishing/tag-library/2.2/n-ihv0.html">ici</a> pour de plus amples explications. Pour les citations, ce champ est équivalent à "Collection". Il est à tord utilisé à la place de "Collection" pour certains types de document (ex. Enregistrement audio, Carte).</td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Nb de Volumes</td>
<td>Nombre de volumes dans une œuvre collective</td>
</tr>
<tr class="odd">
<td>Nb de Pages</td>
<td>Nombre de pages dans un livre</td>
</tr>
<tr class="even">
<td>Section</td>
<td>Section d'un journal</td>
</tr>
<tr class="odd">
<td>Lieu</td>
<td>Lieu de publication d'un document</td>
</tr>
<tr class="even">
<td>Maison d'édition</td>
<td>Maison d'édition d'un document</td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Abrév. de revue</td>
<td>Titre abrégé d'une revue. Quand vous utilisez le module Zotero pour Word ou LibreOffice, Zotero peut automatiquement abréger les titres en utilisant la liste de MEDLINE. Utilisez ce champ pour entrer les abréviations basées sur d'autres systèmes ou générer des bibliographies hors d'un logiciel de traitemetn de texte.</td>
</tr>
<tr class="odd">
<td>ISBN</td>
<td><a href="https://fr.wikipedia.org/wiki/International_Standard_Book_Number">International Standard Book Number</a> du livre ou d'une autre publication</td>
</tr>
<tr class="even">
<td>ISSN</td>
<td><a href="https://fr.wikipedia.org/wiki/International_Standard_Serial_Number">International Standard Serial Number</a> d'un périodique ou d'une publication en série</td>
</tr>
</tbody>
</table>

### Champs relatifs à la date et au mode de consultation des documents

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Consulté le</td>
<td>Date à laquelle une ressource électronique a été consultée. Généralement rempli automatiquement. Vous pouvez saisir "aujourd'hui", "hier" et "demain" pour entrer rapidement la date correspondante.</td>
</tr>
<tr class="even">
<td>DOI</td>
<td>Le <a href="https://fr.wikipedia.org/wiki/Digital_Object_Identifier">Digital Object Identifier</a> (littéralement « identifiant numérique d'objet ») d'un document. Pour les documents autres que les articles de revue, le DOI peut être stocké dans le champ Extra. Voir <a href="#citer_des_champs_a_partir_du_champ_extra">Citer des champs à partir du champ Extra</a> ci-dessous.</td>
</tr>
<tr class="odd">
<td>URL</td>
<td>URL (adresse web) à laquelle le document complet a été consulté. N'utilisez pas ce champ pour des liens vers des notices de catalogue (par exemple, des bibliothèques ou Google Books) ou des résumés - ceux-là doivent plutôt être ajoutés en tant que liens.</td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td>Archive</td>
<td>Principalement pour les documents d'archives, l'archive dans laquelle un document a été trouvé. Également utilisé pour les dépôts, tels que les bases de données de rapports gouvernementaux, les dépôts institutionnels ou les dépôts thématiques.</td>
</tr>
<tr class="even">
<td>Loc. dans l'archive</td>
<td>L'emplacement d'un document dans une archive, tel qu'un numéro de boîte et de dossier ou d'autres informations pertinentes sur l'emplacement issues de l'instrument de recherche. Indiquez dans ce champ le numéro de sous-collection/la cote, le numéro de boîte et le numéro de dossier ensemble. Pour des conseils supplémentaires sur la citation de sources d'archives dans Zotero, voir <a href="https://web.archive.org/web/20150220133939/http://guides.library.harvard.edu/content.php?pid=535700&amp;sid=4445138">la page "How to use Zotero for Archival Research" sur le site des bibliothèques d'Harvard (page en anglais)</a>.</td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Catalogue de bibl.</td>
<td>Le catalogue ou la base de données d'où un document a été importé. Ce champ est utilisé, par exemple, dans le style bibliographique MLA. Ce champ est utilisé plus largement que pour les seuls catalogues de bibliothèque.</td>
</tr>
<tr class="odd">
<td>Cote</td>
<td>La cote d'un document dans une bibliothèque. Pour citer des sources d'archives incluez également la cote dans <code>Loc. dans l'archive</code> (si applicable).</td>
</tr>
</tbody>
</table>

### Champs pour les rapports et les thèses

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Type<br />
Type de rapport</td>
<td>Le type de rapport (par exemple, "Rapport technique", "Dossier technique CPTO") ou de thèse (par exemple, "Thèse de doctorat", "Mémoire de maîtrise" ; le mot "thèse" ou "mémoire" doit être inclus si applicable).</td>
</tr>
<tr class="even">
<td>N° du rapport</td>
<td>Le numéro d'un rapport. N'incluez pas les mots "Nombre", "No.", "Nº", "№", ou équivalents ; ceux-ci seront ajoutés automatiquement par le style bibliographique.</td>
</tr>
<tr class="odd">
<td>Institution<br />
Université</td>
<td>L'institution qui publie un rapport ou l'université qui délivre le diplôme pour une thèse.</td>
</tr>
</tbody>
</table>

### Champs pour les présentations et les conférences

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Titre des actes</td>
<td>Le titre des actes de conférence dans lequel un article de conférence a été publié.</td>
</tr>
<tr class="even">
<td>Intitulé du colloque<br />
Intitulé de la réunion</td>
<td>Le nom de la conférence ou de la réunion où un article ou une présentation a été présenté.</td>
</tr>
<tr class="odd">
<td>Lieu</td>
<td>Pour les "présentations", le lieu où la réunion s'est tenue ou le lieu où la présentation a été faite. Pour les "Articles de colloque" (publiés dans des actes de conférence), utilisez ce champ pour le lieu de publication des actes. Si des lieux distincts sont nécessaires pour le lieu de publication et le lieu de la conférence, laissez ce champ vide et ajoutez les champs <code>Event Place</code> et <code>Publisher Place</code> dans le champ Extra (voir <a href="#citer_des_champs_a_partir_du_champ_extra">Citer des champs à partir du champ Extra</a> ci-dessous).</td>
</tr>
<tr class="even">
<td>Type</td>
<td>Type ou format de la présentation (par exemple, "Poster", "Symposium", "Conférence invitée").</td>
</tr>
</tbody>
</table>

### Champs pour les enregistrements et les émissions

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Format<br />
Type de fichier</td>
<td>Le format d'un enregistrement audio ou vidéo (par exemple "DVD," "CD," "MP3," etc.).</td>
</tr>
<tr class="even">
<td>Durée</td>
<td>La durée de tout type d'enregistrement. Entrez avec l'unité (par exemple, 120 min).</td>
</tr>
<tr class="odd">
<td>Titre du programme</td>
<td>Le titre d'une émission de radio ou de télévision dont un épisode fait partie (par exemple, "This American Life" ou "60 Minutes").</td>
</tr>
<tr class="even">
<td>N° de l'épisode</td>
<td>Le numéro d'épisode d'un podcast ou d'une émission de télévision/radio.</td>
</tr>
<tr class="odd">
<td>Réseau</td>
<td>Le réseau qui diffuse une émission de télévision ou de radio.</td>
</tr>
<tr class="even">
<td>Label</td>
<td>Le label d'enregistrement qui distribue un enregistrement audio sur un marché.</td>
</tr>
<tr class="odd">
<td>Distributeur</td>
<td>L'organisation qui distribue un film sur un marché.</td>
</tr>
<tr class="even">
<td>Studio</td>
<td>Le studio qui produit un enregistrement vidéo.</td>
</tr>
<tr class="odd">
<td>Genre</td>
<td>Le genre d'un film (par exemple, "horreur").</td>
</tr>
</tbody>
</table>

### Champs pour les images, les œuvres d'art et les cartes

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Support de l'illustration</td>
<td>Le type d'œuvre ou de figure ou le support avec lequel elle a été créée (par exemple, "Peinture à l'aquarelle", "Sculpture sur bois", "Cristallographe à rayons X", "Nuage de points").</td>
</tr>
<tr class="even">
<td>Taille d'illustration</td>
<td>Les dimensions de l’œuvre d'art ou de la figure.</td>
</tr>
<tr class="odd">
<td>Échelle</td>
<td>L'échelle d'une carte ou d'un modèle.</td>
</tr>
<tr class="even">
<td>Type</td>
<td>Pour les cartes, description du type ou du genre spécifique de la carte.</td>
</tr>
</tbody>
</table>

### Champs pour les sources primaires et les communications personnelles

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Média</td>
<td>Pour les interviews, format dans lequel une interview a été enregistrée (par exemple, "Enregistrement audio", "Enregistrement vidéo", "Transcription").</td>
</tr>
<tr class="even">
<td>Type</td>
<td>Pour les lettres, description du type ou de l'objet spécifique de la lettre (par exemple, "Correspondance privée"). Pour les manuscrits, description du type ou du statut du manuscrit (par exemple, "Manuscrit non publié", "Manuscrit soumis pour publication", "Working paper").</td>
</tr>
<tr class="odd">
<td>Sujet</td>
<td>Le titre d'un courriel.</td>
</tr>
</tbody>
</table>

### Champs pour les sites web

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Titre du site web<br />
Titre du blog</td>
<td>Le titre du blog ou du site web contenant un article ou une page web spécifique.</td>
</tr>
<tr class="even">
<td>Titre du forum/listserv</td>
<td>Le forum ou le gestionnaire de liste de diffusion sur lequel un message a été posté.</td>
</tr>
<tr class="odd">
<td>Type de site web</td>
<td>Rarement utilisé. Décrit le genre d'une page web, comme "blog personnel" ou "intranet".</td>
</tr>
<tr class="even">
<td>Type d'article</td>
<td>Description du type spécifique de message de forum (par exemple, "Tweet" ou "commentaire Facebook").</td>
</tr>
</tbody>
</table>

### Champs pour les programmes informatiques

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Version</td>
<td>La version d'un programme informatique.</td>
</tr>
<tr class="even">
<td>Système</td>
<td>Le système d'exploitation ou la plateforme pour laquelle un programme informatique est écrit.</td>
</tr>
<tr class="odd">
<td>Société</td>
<td>L'organisation qui publie un programme informatique.</td>
</tr>
<tr class="even">
<td>Langage de programmation</td>
<td>Le langage de programmation dans lequel un programme informatique est écrit.</td>
</tr>
</tbody>
</table>

### Champs supplémentaires

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Autorisations*</td>
<td>Les conditions de copyright, la licence ou le statut de diffusion d'un document.</td>
</tr>
<tr class="even">
<td>Date d'ajout</td>
<td>La date et l'heure auxquelles un document a été ajouté à Zotero. Ce champ est complété automatiquement.</td>
</tr>
<tr class="odd">
<td>Date de modification</td>
<td>La date et l'heure de la dernière modification d'un document dans Zotero. Ce champ est complété automatiquement.</td>
</tr>
<tr class="even">
<td>Extra</td>
<td>Champ libre pour le stockage d'informations supplémentaires. Vous pouvez également stocker des variables supplémentaires qui ne sont pas incluses dans les champs d'un document et qui peuvent être utilisées lors de la création de citations et de bibliographies. Voir <a href="#citer_des_champs_a_partir_du_champ_extra">Citer des champs à partir du champ Extra</a></td>
</tr>
</tbody>
</table>

### Champs pour les documents juridiques

Pour une prise en charge supplémentaire et plus flexible pour la citation des documents juridiques, voir [Citations juridiques (page en anglais)](https://www.zotero.org/support/kb/legal_citations).

##### Lois et audiences

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Nom de l'acte</td>
<td>Le titre complet d'une loi.</td>
</tr>
<tr class="even">
<td>N° de projet</td>
<td>Le numéro d'identification attribué à une proposition de loi.</td>
</tr>
<tr class="odd">
<td>Code</td>
<td>Le nom du code dans lequel un projet de loi ou une loi est publié.</td>
</tr>
<tr class="even">
<td>Volume de code</td>
<td>Le numéro de volume du code contenant un projet de loi ou une loi.</td>
</tr>
<tr class="odd">
<td>N° de code</td>
<td>Le numéro d'identification attribué à un acte juridique dans le code dans lequel il est publié.</td>
</tr>
<tr class="even">
<td>N° officiel de l'acte</td>
<td>Le numéro d'identification attribué à un acte juridique promulgué. Voir <a href="https://www.loc.gov/law/help/statutes.php">cette page pour plus de détails concernant les statuts de législation aux Etats-Unis (page en anglais)</a>.</td>
</tr>
<tr class="odd">
<td>Promulgué le</td>
<td>La date à laquelle une loi a été promulguée.</td>
</tr>
<tr class="even">
<td>Section</td>
<td>La section d'un projet de loi ou d'une loi qui est cité..</td>
</tr>
<tr class="odd">
<td>Commission</td>
<td>La commission qui tient une audition.</td>
</tr>
<tr class="even">
<td>N° du document</td>
<td>Le numéro d'identification de document, attribué à la transcription ou au compte rendu publié d'une audience de commission.</td>
</tr>
<tr class="odd">
<td>Pages</td>
<td>Les pages du volume de code contenant un projet de loi ou une loi.</td>
</tr>
<tr class="even">
<td>Corps législatif*</td>
<td>Le corps législatif (parlement, sénat, etc.) qui débat d'un projet de loi, tient une audience ou adopte une loi.</td>
</tr>
<tr class="odd">
<td>Session</td>
<td>Session d'un corps législatif au cours de laquelle un projet de loi est présenté, une loi est adoptée ou une audience est tenue.</td>
</tr>
<tr class="even">
<td>Histoire</td>
<td>Ressources liées à l'historique de la procédure d'un projet de loi ou d'une loi.</td>
</tr>
</tbody>
</table>

##### Affaires

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Histoire</td>
<td>Ressources liées à l'histoire procédurale d'une affaire judiciaire.</td>
</tr>
<tr class="even">
<td>Nom de l'affaire</td>
<td>Le titre d'une affaire judiciaire.</td>
</tr>
<tr class="odd">
<td>Tribunal</td>
<td>Le tribunal où une affaire judiciaire a été plaidée.</td>
</tr>
<tr class="even">
<td>Date de décision</td>
<td>La date à laquelle une affaire judiciaire a été décidée.</td>
</tr>
<tr class="odd">
<td>N° de requête</td>
<td>Le numéro de requête attribué à une affaire judiciaire à entendre.</td>
</tr>
<tr class="even">
<td>Recueil</td>
<td>Le recueil dans lequel une affaire judiciaire est publiée.</td>
</tr>
<tr class="odd">
<td>Volume du recueil</td>
<td>Le numéro de volume du recueil dans lequel une affaire judiciaire est publiée.</td>
</tr>
<tr class="even">
<td>Première page</td>
<td>La première page du volume de recueil dans lequel figure une affaire.</td>
</tr>
</tbody>
</table>

##### Brevets

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Pays</td>
<td>Le pays qui délivre un brevet.</td>
</tr>
<tr class="even">
<td>Cessionnaire</td>
<td>L'entité à laquelle sont cédés les droits de propriété d'un brevet.</td>
</tr>
<tr class="odd">
<td>Autorité émettrice</td>
<td>L'autorité ou le bureau qui examine la demande et délivre le brevet.</td>
</tr>
<tr class="even">
<td>N° de brevet</td>
<td>Le numéro d'identification attribué à un brevet.</td>
</tr>
<tr class="odd">
<td>Date d'archivage</td>
<td>La date à laquelle une demande de brevet a été déposée.</td>
</tr>
<tr class="even">
<td>Date de parution</td>
<td>La date à laquelle un brevet est officiellement délivré.</td>
</tr>
<tr class="odd">
<td>N° d'application</td>
<td>Le numéro d'identification attribué à une demande de brevet.</td>
</tr>
<tr class="even">
<td>Numéros de priorité</td>
<td>Le numéro de demande international d'un brevet, utilisé pour les revendications de droits de priorité.</td>
</tr>
<tr class="odd">
<td>Références</td>
<td>Ressources liées à l'histoire d'un brevet.</td>
</tr>
<tr class="even">
<td>Statut légal</td>
<td>Le statut juridique d'un brevet ou d'une demande.</td>
</tr>
</tbody>
</table>

## Créateurs des documents

Les types de créateurs marqués d'un astérisque (\*) ne peuvent pas être utilisés dans les citations.

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Auteur</td>
<td>L'auteur ou le créateur principal d'une œuvre. Entrez les auteurs (et autres créateurs) dans l'ordre dans lequel ils doivent être cités.<br />
Entrez le nom des personnes dans le mode à deux champs (Nom, Prénom). Entrez le nom des institutions ou des organisations dans le mode à un champ.</td>
</tr>
<tr class="even">
<td>Éditeur</td>
<td>L'éditeur d'un document ou de la publication plus large dont un document fait partie (par exemple, un livre, une revue). Si une distinction est faite entre l'éditeur textuel et le directeur de la publication, utilisez "Éditeur " pour l'éditeur textuel et entrez le directeur de la publication en tant que "Editorial Director" dans le champ "Extra". Voir <a href="#citer_des_champs_a_partir_du_champ_extra">Citer des champs à partir du champ Extra</a> ci-dessous.</td>
</tr>
<tr class="odd">
<td>Directeur de coll.</td>
<td>L'éditeur supervisant une série de documents.</td>
</tr>
<tr class="even">
<td>Traducteur</td>
<td>Le traducteur d'un document. Si une même personne est à la fois éditrice et traductrice d'une œuvre, indiquez son nom à la fois en tant qu'éditeur et en tant que traducteur dans des champs séparés.</td>
</tr>
<tr class="odd">
<td>Collaborateur*</td>
<td>Autres personnes ou entités associées à une œuvre qui ne doivent pas être incluses dans les citations (par exemple, les personnes figurant après "avec" sur la couverture d'un livre). Utilisé en tant qu'"auteur" pour les documents de type "Audience".</td>
</tr>
<tr class="even">
<td>Auteur recensé</td>
<td>Le nom de l'auteur de l’œuvre qui est recensée.</td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Artiste</td>
<td>Le créateur d'une œuvre d'art ou d'une figure. L'"auteur" pour les documents de type "Illustration".</td>
</tr>
<tr class="odd">
<td>Interprète</td>
<td>La personne qui joue sur un enregistrement audio. L'"auteur" pour les documents de type "Enregistrement audio".</td>
</tr>
<tr class="even">
<td>Compositeur</td>
<td>Le compositeur de la musique dans un enregistrement audio.</td>
</tr>
<tr class="odd">
<td>Paroles de*</td>
<td>La personne qui a écrit les paroles dans un enregistrement audio (par exemple, le parolier ou le rédacteur de discours).</td>
</tr>
<tr class="even">
<td>Membre de la distribution*</td>
<td>Membre de la distribution d'une émission ou d'un spectacle.</td>
</tr>
<tr class="odd">
<td>Metteur en scène</td>
<td>Le directeur d'une émission ou d'un enregistrement. L'"auteur" pour les documents de type "Film", "Enregistrement vidéo", "Émission de radio" et "Émission de télévision".</td>
</tr>
<tr class="even">
<td>Producteur*</td>
<td>Un producteur pour un film, un enregistrement ou une émission.</td>
</tr>
<tr class="odd">
<td>Scénariste*</td>
<td>La personne qui écrit le scénario d'un film ou d'une émission.</td>
</tr>
<tr class="even">
<td>Diffuseur</td>
<td>L'hébergeur d'un podcast. L'"auteur" des documents de type "Balado (Podcast)".</td>
</tr>
<tr class="odd">
<td>Invité*</td>
<td>Un invité à un podcast, une émission de radio ou de télévision.</td>
</tr>
<tr class="even">
<td>Cartographe</td>
<td>Le principal créateur d'une carte. L'"auteur" des documents de type "Carte".</td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Programmeur</td>
<td>Le programmeur d'un programme informatique. L'"auteur" des documents de type "Programme informatique".</td>
</tr>
<tr class="odd">
<td>Présentateur</td>
<td>La personne qui fait une présentation. L'"auteur" des documents de type "Présentation".</td>
</tr>
<tr class="even">
<td>Interviewé</td>
<td>La personne interrogée. L'"auteur" des documents de type "Interview"</td>
</tr>
<tr class="odd">
<td>Reporter</td>
<td>La personne qui pose des questions lors d'une interview.</td>
</tr>
<tr class="even">
<td>Commentateur*</td>
<td>Une personne qui commente un article de blog.</td>
</tr>
<tr class="odd">
<td>Destinataire</td>
<td>La personne qui reçoit une lettre, un courriel ou un message.</td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td>Auteur</td>
<td>L'auteur principal d'un projet de loi ou d'un texte de loi. L'"auteur" pour les documents de type "Projet/proposition de loi" et "Acte juridique" .</td>
</tr>
<tr class="even">
<td>Co-parrain*</td>
<td>Co-parrains ou partisans d'un projet de loi ou d'un texte de loi.</td>
</tr>
<tr class="odd">
<td>Conseiller*</td>
<td>L'avocat qui plaide une affaire judiciaire.</td>
</tr>
<tr class="even">
<td>Inventeur</td>
<td>Le créateur d'une invention. L'"auteur" pour les documents de type "Brevet".</td>
</tr>
<tr class="odd">
<td>Mandataire/agent*</td>
<td>Le mandataire ou l'agent qui représente un inventeur lors du dépôt d'un brevet.</td>
</tr>
</tbody>
</table>

### Intitulés des rôles pour les créateurs de médias

Pour les citations de films, d'enregistrements et d'émissions, Zotero propose actuellement une prise en charge limitée au référencement des producteurs, des scénaristes et de certains autres rôles de créateurs.

Pour référencer les réalisateurs, laissez le champ principal de Zotero vide (ou entrez les noms en tant que "Contributeur") et entrez le nom des réalisateurs en utilisant `Director` dans Extra. Voir [Citer des champs à partir du champ Extra](#citer_des_champs_a_partir_du_champ_extra) ci-dessous.

Tous les rôles de créateur (réalisateur, producteur, scénariste, etc.) peuvent également être référencés en entrant le nom à l'aide du rôle "auteur" par défaut pour le document (*interprète* pour un enregistrement audio, *diffuseur* pour une baladodiffusion, et // metteur en scène// pour un film, une émission de radio ou de télévision, et un enregistrement vidéo) et en ajoutant l'intitulé approprié entre parenthèses après le prénom ou le nom de famille des auteur, en fonction de votre style bibliographique , par exemple MacNaughton || Ian (Producteur) pour le style APA. Notez que les intitulés seront rendus mot pour mot dans les citations ; entrez des termes abrégés (par exemple, "Prod.") ici si nécessaire.

Si le style utilise des initiales pour le prénom des auteurs plutôt que la forme complète des noms (par exemple le style APA), si l'intitulé contient plusieurs mots (par exemple, "Producteur exécutif" ou "Écrivain et réalisateur"), Zotero abrégera les mots de l'intitulé suivant le premier mot. Pour éviter cela, tapez un caractère "Word Joiner" (Unicode U+2060, affiché ici entre guillemets : "") de chaque côté de chaque espace de l'intitulé.

Voir aussi [Les rôles de créateur de médias (page en anglais)](https://www.zotero.org/support/kb/media_creator_roles).

## Types de documents et champs supplémentaires

### Types de documents citables non inclus dans Zotero

Ces types de documents ne sont pas encore officiellement pris en charge dans Zotero. À des fins de citation, vous pouvez convertir un document d'un type différent en l'un de ces types en le saisissant dans le [champ Extra](#citer_des_champs_a_partir_du_champ_extra) au format suivant :

    Type: Type CSL

Par exemple :

    Type: dataset

<table>
<thead>
<tr class="header">
<th>Type de document</th>
<th>Type CSL</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Jeu de données</td>
<td><code>dataset</code></td>
<td>Un jeu de données brut.</td>
</tr>
<tr class="even">
<td>Figure</td>
<td><code>figure</code></td>
<td>Une figure incluse dans un écrit scientifique ou académique.</td>
</tr>
<tr class="odd">
<td>Partition musicale</td>
<td><code>musical_score</code></td>
<td>La partition écrite d'une œuvre musicale.</td>
</tr>
<tr class="even">
<td>Pamphlet</td>
<td><code>pamphlet</code></td>
<td>Un ouvrage publié de manière informelle. Généralement plus petit et moins technique qu'un rapport.</td>
</tr>
<tr class="odd">
<td>Recension de livre</td>
<td><code>review-book</code></td>
<td>Une recension d'un livre. Saisissez-la en tant qu'article de revue, de magazine ou de journal, selon son mode de publication, en indiquant un créateur de type "Auteur recensé".</td>
</tr>
<tr class="even">
<td>Traité</td>
<td><code>treaty</code></td>
<td>Un traité juridique entre deux nations.</td>
</tr>
</tbody>
</table>

### Champs citables non inclus dans Zotero

Ces champs ne sont pas encore officiellement pris en charge dans Zotero. À des fins de citation, vous pouvez ajouter un ou plusieurs de ces champs à un document Zotero en le(s) saisissant dans le [champ Extra](#citer_des_champs_a_partir_du_champ_extra) au format suivant :

    Variable CSL: valeur

Par exemple :

    PMID: 123456
    Statut: in press
    Date originale: 1886-04-01
    Director: Kubrick || Stanley

<table>
<thead>
<tr class="header">
<th>Champ</th>
<th>Variable CSL</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>PMID</td>
<td><code>PMID</code></td>
<td>L'identifiant PubMed.</td>
</tr>
<tr class="even">
<td>PMCID</td>
<td><code>PMCID</code></td>
<td>L'identifiant PubMed Centra.</td>
</tr>
<tr class="odd">
<td>Statut</td>
<td><code>status</code></td>
<td>Le statut de publication d'un document (par exemple, "à paraître", "sous presse", "publication en ligne anticipée").</td>
</tr>
<tr class="even">
<td>Date de soumission</td>
<td><code>Submitted</code></td>
<td>La date à laquelle un document a été soumis pour publication.</td>
</tr>
<tr class="odd">
<td>Titre recensé</td>
<td><code>Reviewed Title</code></td>
<td>Le titre d'une œuvre recensée.</td>
</tr>
<tr class="even">
<td>Numéro de chapiter</td>
<td><code>Chapter Number</code></td>
<td>Le numéro d'un chapitre à l'intérieur d'un livre.</td>
</tr>
<tr class="odd">
<td>Lieu de l'archive</td>
<td><code>Archive Place</code></td>
<td>L' emplacement géographique d'une archive.</td>
</tr>
<tr class="even">
<td>Date de l'événement</td>
<td><code>Event Date</code></td>
<td>La date à laquelle un événement a eu lieu. A saisir au format ISO (année-mois-jour).</td>
</tr>
<tr class="odd">
<td>Lieu de l'événement</td>
<td><code>Event Place</code></td>
<td>L' emplacement géographique d'un événement.</td>
</tr>
<tr class="even">
<td>Date originale</td>
<td><code>Original Date</code></td>
<td>La date originale à laquelle un document a été publié. A saisir au format ISO (année-mois-jour).</td>
</tr>
<tr class="odd">
<td>Titre original</td>
<td><code>Original Title</code></td>
<td>Le titre original d'une œuvre (par exemple, le titre non traduit).</td>
</tr>
<tr class="even">
<td>Editeur original</td>
<td><code>Original Publisher</code></td>
<td>L'éditeur commercial de la version originale d'un document (par exemple, la version non traduite).</td>
</tr>
<tr class="odd">
<td>Lieu de publication original</td>
<td><code>Original Publisher Place</code></td>
<td>L'emplacement géographique de l'éditeur commercial de la version originale d'un document (par exemple, la version non traduite).</td>
</tr>
<tr class="even">
<td>Auteur original</td>
<td><code>Original Author</code></td>
<td>Un type de créateur. Le créateur original d'une oeuvre.</td>
</tr>
<tr class="odd">
<td>Metteur en scène</td>
<td><code>Director</code></td>
<td>Un type de créateur. Le réalisateur d'un film, d'un enregistrement ou d'une émission. Dans Zotero, "Metteur en scène" correspond à l'auteur en CSL. Si vous avez besoin de mentionner un intitulé pour les metteurs en scène —"(Dir.)", saisissez le nom du metteur en scène dans le champ Extra.</td>
</tr>
<tr class="even">
<td>Directeur de la publication</td>
<td><code>Editorial Director</code></td>
<td>Un type de créateur. En France, le <a href="https://fr.wikipedia.org/wiki/Directeur_de_la_publication">directeur de la publication</a> désigne le responsable, y compris pénalement, du contenu d'une publication diffusée par voie de presse, d’audiovisuel, ou de communication numérique.</td>
</tr>
<tr class="odd">
<td>Illustrateur</td>
<td><code>Illustrator</code></td>
<td>Un type de créateur. L'illustrateur d'une œuvre.</td>
</tr>
</tbody>
</table>

### Citer des champs à partir du champ Extra

Si un type de document Zotero manque de certains champs nécessaires pour les citations, il est possible d'ajouter ces champs dans le champ Extra.

Saisissez chaque variable sur une ligne séparée en haut du champ Extra dans le format suivant :

    Variable CSL: valeur

Par exemple:

    DOI: 10.1128/AEM.02591-07
    Date originale: 1824
    PMCID: PMC3531190

À l'exception des variables pour le type de documents (CSL `type`) et les dates (CSL `issued`, etc.), les variables saisies dans le champ Extra ne remplaceront pas les valeurs correspondantes saisies dans les champs Zotero appropriés.

#### Dates

Les dates saisies dans le champ Extra remplaceront la date saisie dans le champ Date de Zotero. Les dates doivent être saisies au format ISO (année-mois-jour). Des plages de dates peuvent être saisies dans ce format :

    Issued: 2001-12-15/2001-12-31

#### Noms

Pour les variables de créateur, séparez les noms à deux champs (nom et prénom) saisis dans le champ Extra avec deux caractères de barre verticale ("||"), comme ceci :

    Editorial Director: De Gaulle || Charles

![](tag>kb-fr entry-fr)
