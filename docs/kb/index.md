# Base de connaissance

La base de connaissance contient un grand nombre de questions fréquemment posées par les utilisateurs et utilisatrices de Zotero.

Naviguez dans la colonne de gauche pour chercher si une question répond à votre besoin. Dans le cas contraire, vous pouvez solliciter de l'aide sur le [forum de Zotero](../forum_guidelines.md).

!!! Caution "En cours de traduction"

    La base de connaissance dans la documentation anglophone de Zotero contient plus d'une centaine de pages. Nous sommes encore en train de les traduire. Ainsi il est possible que la question pour laquelle vous cherchez une réponse ait la mention `[EN]`. Dans ce cas la traduction n'existe pas encore et vous serez redirigé·e vers la version originale en anglais sur le site de Zotero.
