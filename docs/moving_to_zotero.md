# Importer depuis d'autres logiciels de gestion bibliographique

Il est possible d'importer dans Zotero les données provenant d'autres logiciels. Voici les procédures pour les logiciels les plus populaires :

-   [Mendeley](https://www.zotero.org/support/kb/mendeley_import)
-   [Endnote](./kb/importing_records_from_endnote.md)
-   [Citavi](https://www.zotero.org/support/kb/import_from_citavi)
-   [Fichiers textes (par ex. Word)](./kb/importing_formatted_bibliographies.md)
-   [Formats standardisés (tels que Bib(La)TeX ou JabRef)](./kb/importing_standardized_formats.md)

Il est aussi possible d'importer les données provenant d'autres outils, tels que Reference Manager, RefWorks, Papers, Google Scholar Library, ReadCube, etc. Il faut d'abord exporter les données dans un [format](./kb/importing_standardized_formats.md) tel que RIS, BibTeX, CSL JSON, puis d'importer ce fichier dans Zotero en sélectionnant Ficher → “Importer…” puis choisir "Un fichier".

Si vous souhaitez récupérer les données d'un installation de Zotero sur un autre ordinateur, consultez la page [Comment puis-je transférer ma bibliothèque Zotero vers un autre ordinateur ?](./kb/transferring_a_library.md).
