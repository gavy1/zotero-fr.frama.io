# Documentation Zotero francophone

Ce dépôt contient le code source du site de la documentation Zotero francophone.

Le site est accessible à l'adresse : https://docs.zotero-fr.org.
---

## Informations techniques

La documentation utilise [MkDocs](http://www.mkdocs.org) ([documentation](https://www.mkdocs.org/getting-started/)) avec le thème [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) ([documentation](https://squidfunk.github.io/mkdocs-material/getting-started/)).

Elle est hébergée sur [Framagit.org](https://framagit.org) en utilisant [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) à partir du [ce modèle](https://gitlab.com/pages/mkdocs).

### GitLab CI

La documentation est publiée automatiquement via l'[intégration continue de GitLab](https://about.gitlab.com/gitlab-ci/) en utilisant les étapes définies dans [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Contribuer à la documentation

Le [wiki dans le dépôt Framagit](https://framagit.org/zotero-fr/documentation-zotero-francophone/-/wikis/home) détaille la syntaxe spécifique à la rédaction de la documentation et conment résoudre les problèmes les plus fréquents.

### Développer localement

Pour travailler localement sur la documentation, suivre ces étapes :

1. Forker, cloner ou télécharger ce dépôt,
2. Installer [Python](https://www.mkdocs.org/user-guide/installation/#installing-python) et éventuellement [Pip](https://www.mkdocs.org/user-guide/installation/#installing-pip),
3. [Installer Material for MkDocs](https://squidfunk.github.io/mkdocs-material/getting-started/),
4. Générer le site en utilisant la commande : `mkdocs serve` (le site est alors accessible à l'adresse `localhost:8000`),
5. Ajouter ou modifier du contenu (le site en local est automatiquement rechargé pour tenir compte des changements),
6. Commiter les changements et les pousser sur le dépôt Framagit.

### Développer en ligne

Il est possible de travailler sur la documentation directement depuis Framagit.org, en utilisant le [Web IDE de GitLab](https://docs.gitlab.com/ee/user/project/web_ide/).
